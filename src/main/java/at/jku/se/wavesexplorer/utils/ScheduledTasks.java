package at.jku.se.wavesexplorer.utils;

import at.jku.se.wavesexplorer.model.Block;
import at.jku.se.wavesexplorer.model.Peer;
import at.jku.se.wavesexplorer.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ScheduledTasks {

    @Autowired
    private BlockService blockService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AliasService aliasService;

    @Autowired
    private AssetService assetService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private PeerService peerService;

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private static final boolean RESET_DB = false;
    private static boolean deleted = false;
    private boolean isFillingCurrently = true;

    @Scheduled(fixedDelay = 6000)
    public void fillDBTask() {
        if (RESET_DB && !deleted) {
            deleteDb();
            deleted = true;
        }
        fillDb();
    }

    /**
     * This method fills the database from the existing height to the actual height.
     */
    private void fillDb() {
        if (!isFillingCurrently) {
            WavesService wavesService = new WavesService(addressService, aliasService, transactionService, assetService);
            insertBlocks(wavesService);
        }
    }

    /**
     * erster Block hat keine reference (nötig?); sonst alles in block richtig vorhanden
     * es werden maximal 100 transactionen pro block gespeichert
     *
     * @param wavesService
     */
    private void insertBlocks(WavesService wavesService) {
        int curWavesBlockHeight = wavesService.getBlocksHeight();
        //curWavesBlockHeight = 20000;   436147                              // TODO: hier einstellen wieviele Blöcke höchstens heruntergeladen werden sollen
        long curDbBlockHeight = 0;
        try {
            curDbBlockHeight = blockService.findMaxHeight();
        } catch (Exception e) {
        }

        log.info("Datenbank Blockhöhe:    " + curDbBlockHeight);
        log.info("Waves Server Blockhöhe: " + curWavesBlockHeight);

        if (curDbBlockHeight < curWavesBlockHeight) {
            isFillingCurrently = true;
            Block last = null;
            for (long i = curDbBlockHeight + 1; i <= curWavesBlockHeight; i += 1/*100*/) {
                Block block = wavesService.getBlockAtHeight(i);
                log.info("Got next block -> height = " + i);

                if (last == null && curDbBlockHeight > 0) {
                    last = blockService.findByHeight(curDbBlockHeight);
                }
                block.setReference(last);
                blockService.create(block);
                last = block;
            }
            log.info("Got all blocks!");

            int i = 0;
            Set<Peer> allPeers = wavesService.getPeers();

            if (allPeers.size() > peerService.count()) {
                for (Peer peer : allPeers) {
                    if (!peerService.exists(peer.getId())) {
                        peerService.create(peer);
                        log.info(String.format("Persisted %d peer(s)", ++i));
                    }
                }
            }

            log.info("Persisted all!");
            isFillingCurrently = false;
        }
    }

    private void deleteDb() {
        try {
            blockService.deleteAll();
            log.info("deleted block table data");
        }catch (Exception e) {
            log.error("blockservice delete failed", e);
        }
        try {
            addressService.deleteAll();
            log.info("deleted address table data");
        }catch (Exception e) {
            log.error("addressservice delete failed", e);
        }
        try {
            transactionService.deleteAll();
            log.info("deleted transaction table data");
        } catch (Exception e) {
            log.error("transactionservice delete failed", e);
        }
        try {
            peerService.deleteAll();
            log.info("deleted peer table data");
        } catch (Exception e) {
            log.error("peerservice delete failed", e);
        }
    }
}