package at.jku.se.wavesexplorer.utils;

import at.jku.se.wavesexplorer.model.*;
import at.jku.se.wavesexplorer.service.AddressService;
import at.jku.se.wavesexplorer.service.AliasService;
import at.jku.se.wavesexplorer.service.AssetService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WavesService {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    /************************************** RestApi URLs **************************************/

    private static final String BASIC_URL = "http://127.0.0.1:6869";

    /************************************** BLOCKS  *******************************************/
    // Return the current blockchain height
    private static final String URL_GET_BLOCKS_HEIGHT = "/blocks/height";

    // Return the last block data
    private static final String URL_GET_BLOCKS_LAST = "/blocks/last";

    // Return block data at the given height
    private static final String URL_GET_BLOCKS_AT_HEIGHT = "/blocks/at/{height}";

    // Return block data from height to height
    private static final String URL_GET_BLOCKS_FROM_TO = "/blocks/seq/{from}/{to}";

    // Return block data by a specified Base58encoded signature
    private static final String URL_GET_BLOCKS_SIGNATURE= "/blocks/signature/{signature}";

    // Return block data from height to height
    private static final String URL_GET_BLOCKS_ADDRESS_FROM_TO = "/blocks/address/{address}/{from}/{to}";

    /************************************** ADDRESSES *****************************************/
    // Get list of all accounts addresses in the node's wallet.
    private static final String URL_GET_ALL_ADDRESSES = "/addresses";

    // Get account balance in WAVES in {address}.
    private static final String URL_GET_BALANCE_FROM_ADDRESS = "/addresses/balance/{address}";

    // Get list of accounts addresses with indexes at this range in the node's wallet.
    private static final String URL_GET_FROM_TO_ADDRESSES = "/addresses/seq/{from}/{to}";

    // Get account balance in WAVES by {address} after {confirmations} from now.
    private static final String URL_GET_BALANCE_FROM_ADDRESS_CONFIRMATIONS = "/addresses/balance/{address}/{confirmations}";

    /************************************** TRANSACTIONS **********************************/
    private static final String URL_GET_TRANSACTIONS_FROM_ADDRESS_WITH_LIMIT = "/transactions/address/{address}/limit/{limit}";

    /************************************** PEERS *****************************************/
    // Returns list of all ever known not blacklisted peers with publicly available declared address.
    private static final String URL_GET_ALL_PEERS = "/peers/all";

    // Returns list of all ever known not blacklisted peers with publicly available declared address.
    private static final String URL_GET_PEERS_BLACKLIST = "/peers/blacklisted";

    // Returns list of all currently connected peers to the node.
    private static final String URL_GET_PEERS_CONNECTED = "/peers/connected";

    // Returns list of all currently connected peers to the node.
    private static final String URL_POST_PEERS_CONNECT = "/peers/connect";

    /************************************** ASSET *****************************************/
    private static final String URL_GET_ASSET_BALANCE_BY_ADDRESS = "/assets/balance/{address}";

    /************************************** ALIAS ****************************************/
    private static final String URL_GET_ALIAS_BY_ADDRESS = "/alias/by-address/{address}";

    private AddressService addressService;
    private AliasService aliasService;
    private TransactionService transactionService;
    private AssetService assetService;

    public WavesService(AddressService addressService, AliasService aliasService, TransactionService transactionService, AssetService assetService) {
        this.addressService = addressService;
        this.aliasService = aliasService;
        this.transactionService = transactionService;
        this.assetService = assetService;
    }

    /**
     * Get all addresses.
     * @return
     */
    public List<Address> getAllAddresses() {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_ALL_ADDRESSES);
        if (jsonResults == null) return null;

        ArrayList<Address> addresses = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(jsonResults);

            for (int i = 0; i <jsonArray.length(); i++) {
                String addressName = jsonArray.getString(i);
                addresses.add(new Address(addressName));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return addresses;
    }

    /**     *
     * Sets the balance and the confirmations of a certain address.
     * @param addressStr
     * @return
     */
    public Address getBalanceFromAddress(String addressStr) {
        return getBalanceFromAddress(new Address(addressStr));
    }

    /**
     * Sets the balance and the confirmations of a certain address.
     * @param address
     * @return
     */
    public Address getBalanceFromAddress(Address address) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_BALANCE_FROM_ADDRESS.replace("{address}", address.getAddress()));
        if (jsonResults == null) return null;

        try {
            JSONObject jsonObject = new JSONObject(jsonResults);
            if (jsonObject.has("balance")) {
                address.setBalance(jsonObject.getDouble("balance"));
            }
            if (jsonObject.has("confirmations")) {
                address.setConfirmations(jsonObject.getInt("confirmations"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return address;
    }

    /**
     * This method returns the height of the blockchain.
     * @return
     */
    public int getBlocksHeight() {
        String jsonResult = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_BLOCKS_HEIGHT);
        if (jsonResult == null) return 0;

        int height = 0;
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            if (jsonObject.has("height")) {
                height = jsonObject.getInt("height");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return height;
    }

    /**
     * This method returns the last block.
     * @return
     */
    public Block getLastBlock() {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_BLOCKS_LAST);
        return getBlockFromJson(jsonResults);
    }

    /**
     * This method returns a Block at the certain height.
     */
    public Block getBlockAtHeight(long height) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_BLOCKS_AT_HEIGHT.replace("{height}", String.valueOf(height)));
        return getBlockFromJson(jsonResults);
    }

    /**
     * Get a list of blocks in the certain sequence and with the certain address.
     * To big sequences are not allowed. Max = 100
     */
    public Set<Block> getBlocksAddresseFromTo(Address address, int from, int to) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL +
                ((URL_GET_BLOCKS_ADDRESS_FROM_TO.replace("{from}", String.valueOf(from)))
                        .replace("{to}", String.valueOf(to))).replace("{address}", address.getAddress()));
        try {
            JSONArray jsonArray = new JSONArray(jsonResults);
            Set<Block> blocks = new HashSet<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                blocks.add(getBlockFromJsonObject(jsonArray.getJSONObject(i)));
            }
            //address.setBlocks(blocks);
            return blocks;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get a list of blocks in the certain sequence.
     * Too big sequences are not allowed. Max = 100
     */
    public HashMap<Long, Block> getBlocksFromTo(int from, int to) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL +
                (URL_GET_BLOCKS_FROM_TO.replace("{from}", String.valueOf(from))).replace("{to}", String.valueOf(to)));
        try {
            JSONArray jsonArray = new JSONArray(jsonResults);
            HashMap<Long, Block> blocks = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                Block b = getBlockFromJsonObject(jsonArray.getJSONObject(i));
                blocks.put(b.getHeight(), b);
            }
            return blocks;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method creates a block object from a json-String.
     */
    private Block getBlockFromJson(String jsonResults) {
        if (jsonResults == null) return null;

        try {
            JSONObject jsonObject = new JSONObject(jsonResults);
            return getBlockFromJsonObject(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method creates a block object from a jsonObject.
     */
    private Block getBlockFromJsonObject(JSONObject jsonObject) throws JSONException {
        Block block = new Block();
        if (jsonObject.has("version")) {
            block.setVersion(jsonObject.getDouble("version"));
        }
        if (jsonObject.has("timestamp")) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(jsonObject.getLong("timestamp"));
            block.setTimestamp(calendar);
        }
        if (jsonObject.has("reference")) {
            // dieses Feld gibt den vorherigen block an
            /*
            Block reference = new Block();
            reference.setSignature(jsonObject.getString("reference"));
            block.setReference(reference);
            */
        }
        if (jsonObject.has("signature")) {
            block.setSignature(jsonObject.getString("signature"));
        }
        if (jsonObject.has("fee")) {
            block.setFee(jsonObject.getDouble("fee"));
        }
        if (jsonObject.has("blocksize")) {
            block.setBlocksize(jsonObject.getInt("blocksize"));
        }
        if (jsonObject.has("generator")) {
            String address = jsonObject.getString("generator");
            Address generator = addressService.find(address);
            if (generator == null) {
                generator = new Address(address);
                block.setGenerator(generator);
                generator = getBalanceFromAddress(generator);
                addressService.create(generator);
                HashSet<Alias> aliasByAddress = getAliasByAddress(generator.getAddress());
                for (Alias alias : aliasByAddress) {
                    alias.setAddress(generator);
                    aliasService.create(alias);
                }
                //block.addTransactions(getTransactionsFromAddressWithLimit(generator, 10000));
                persistAssetBalanceAndAssetTransactionsByAddress(generator, block);
            } else {
                block.setGenerator(generator);
                //addressService.create(generator);
            }
        }
        if (jsonObject.has("height")) {
            block.setHeight(jsonObject.getLong("height"));
        }
        if (jsonObject.has("nxtconsensus")) {
            JSONObject nxtconsensus = jsonObject.getJSONObject("nxtconsensus");
            if (nxtconsensus.has("basetarget")) {
                // basetarget ist vom typ int
            }
            if (nxtconsensus.has("generationsignature")) {
                // generationsignature ist vom typ string
            }
        }
        if (jsonObject.has("transactions")) {
            // now save the transactions and the addresses used in the block
            block.addTransactions(getTransactionsAndPersistAddresses(jsonObject));
        }

        return block;
    }

    /**
     * There are stored transactions and addresses when you try to get the blocks.
     */
    private List<Transaction> getTransactionsAndPersistAddresses(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = jsonObject.getJSONArray("transactions");
        List<Transaction> transactions = new LinkedList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject transactionObj = jsonArray.getJSONObject(i);
            Transaction transaction = getTransactionFromJson(transactionObj);
            transactions.add(transaction);
        }
        return transactions;
    }

    private Transaction getTransactionFromJson(JSONObject transactionObj) throws JSONException {
        Transaction transaction = new Transaction();
        if (transactionObj.has("type")) {
            // https://github.com/wavesplatform/Waves/wiki/Data-Structures
            transaction.setType(transactionObj.getInt("type"));
        }
        if (transactionObj.has("signature")) {
            transaction.setSignature(transactionObj.getString("signature"));
        }
        if (transactionObj.has("id")) {
            transaction.setTransactionId(transactionObj.getString("id"));
        }
        if (transactionObj.has("fee")) {
            transaction.setFee(transactionObj.getDouble("fee"));
        }
        if (transactionObj.has("timestamp")) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(transactionObj.getLong("timestamp"));
            transaction.setTimestamp(calendar);
        }
        if (transactionObj.has("sender")) {
            String address = transactionObj.getString("sender");
            Address sender = addressService.find(address);
            if (sender == null) {
                sender = new Address(address);
                transaction.setSenderAddress(sender);
                sender = getBalanceFromAddress(sender);
                addressService.create(sender);
                HashSet<Alias> aliasByAddress = getAliasByAddress(sender.getAddress());
                for (Alias alias : aliasByAddress) {
                    alias.setAddress(sender);
                    aliasService.create(alias);
                }
                // TODO
                //persistAssetBalanceAndAssetTransactionsByAddress(sender);
            } else {
                transaction.setSenderAddress(sender);
                //addressService.create(sender);
            }
        }
        if (transactionObj.has("senderPublicKey")) {
            transaction.setSenderPublicKey(transactionObj.getString("senderPublicKey"));
        }
        if (transactionObj.has("leaseId")) {
            transaction.setLeaseid(transactionObj.getString("leaseId"));
        }
        if (transactionObj.has("recipient")) {
            String address = transactionObj.getString("recipient");
            log.info(transactionObj.toString());
            Address recipient = addressService.find(address);
            log.info("address: " + recipient);

            if (recipient == null) {
                Alias recipientAlias = aliasService.findById(address);
                if(recipientAlias != null){
                    recipient = recipientAlias.getAddress();
                    log.info("recipient: " + recipient.getAddress());
                }

                if(recipient == null){
                    recipient = new Address(address);
                    transaction.setRecipientAddress(recipient);
                    recipient = getBalanceFromAddress(recipient);
                    addressService.create(recipient);
                    HashSet<Alias> aliasByAddress = getAliasByAddress(recipient.getAddress());
                    for (Alias alias : aliasByAddress) {
                        alias.setAddress(recipient);
                        aliasService.create(alias);
                    }
                }

                //TODO
                //persistAssetBalanceAndAssetTransactionsByAddress(recipient);
            } else {
                transaction.setRecipientAddress(recipient);
                //addressService.create(recipient);
            }
        }
        if (transactionObj.has("amount")) {
            transaction.setAmount(transactionObj.getDouble("amount"));
        }
        return transaction;
    }

    public HashSet<Peer> getPeers() {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL + URL_GET_ALL_PEERS);

        try {
            JSONObject jsonObject = new JSONObject(jsonResults);
            if (jsonObject.has("peers")) {
                JSONArray jsonArray = jsonObject.getJSONArray("peers");
                HashSet<Peer> peers = new HashSet<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    peers.add(getPeersFromJsonObject(jsonArray.getJSONObject(i)));
                }
                return peers;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Peer getPeersFromJsonObject(JSONObject jsonObject) {
        Peer peer = new Peer();
        try {
            if (jsonObject.has("address")) {
                peer.setAddress(jsonObject.getString("address"));
            }
            if (jsonObject.has("declaredAddress")) {
                peer.setDeclaredAddress(jsonObject.getString("declaredAddress"));
            }
            if (jsonObject.has("peerName")) {
                peer.setPrivateName(jsonObject.getString("peerName"));
            }
            if (jsonObject.has("peerNonce")) {
                peer.setPeerNounce(jsonObject.getInt("peerNonce"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return peer;
    }

    public HashSet<Alias> getAliasByAddress(String address) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL +
                (URL_GET_ALIAS_BY_ADDRESS.replace("{address}", address)));
        try {
            JSONArray jsonArray = new JSONArray(jsonResults);
            HashSet<Alias> aliases = new HashSet<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                Alias alias = new Alias();
                alias.setId(jsonArray.getString(i));
                aliases.add(alias);
            }
            return aliases;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void persistAssetBalanceAndAssetTransactionsByAddress(Address address, Block block) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL +
                (URL_GET_ASSET_BALANCE_BY_ADDRESS.replace("{address}", address.getAddress())));
        try {
            JSONObject jsonObject = new JSONObject(jsonResults);
            if (jsonObject.has("balances")) {
                JSONArray jsonArray = jsonObject.getJSONArray("balances");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Asset asset = new Asset();
                    JSONObject assetJson = jsonArray.getJSONObject(i);
                    asset.setAddress(address);
                    if (assetJson.has("assetId")) {
                        asset.setId(assetJson.getString("assetId"));
                    }
                    if (assetJson.has("balance")) {
                        asset.setBalance(assetJson.getDouble("balance"));
                    }
                    if (assetJson.has("reissuable")) {
                        asset.setReissuable(assetJson.getBoolean("reissuable"));
                    }
                    if (assetJson.has("quantity")) {
                        asset.setQuantity(assetJson.getDouble("quantity"));
                    }

                    if (assetJson.has("issueTransaction")) {
                        JSONObject issueTransaction = assetJson.getJSONObject("issueTransaction");
                        Transaction transaction = getTransactionFromJson(issueTransaction);
                        if (transaction != null) {
                            if (issueTransaction.has("name")) {
                                asset.setName(issueTransaction.getString("name"));
                            }
                            if (issueTransaction.has("description")) {
                                asset.setDescription(issueTransaction.getString("description"));
                            }
                            if (issueTransaction.has("quantity")) {
                                asset.setQuantity(issueTransaction.getDouble("quantity"));
                            }
                            if (issueTransaction.has("decimals")) {
                                asset.setDecimals(issueTransaction.getInt("decimals"));
                            }
                            if (issueTransaction.has("reissuable")) {
                                asset.setReissuable(issueTransaction.getBoolean("reissuable"));
                            }
                            if (!assetService.exists(asset.getId())) {
                                assetService.create(asset);
                                transaction.setBlock(block);
                                transaction.setAsset(asset);
                                transactionService.create(transaction);
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Transaction> getTransactionsFromAddressWithLimit(Address address, int limit) {
        String jsonResults = JsonHelper.getJsonFromUrl(BASIC_URL +
                URL_GET_TRANSACTIONS_FROM_ADDRESS_WITH_LIMIT
                        .replace("{address}", address.getAddress())
                        .replace("{limit}", String.valueOf(limit)));

        try {
            JSONArray jsonArray = new JSONArray(jsonResults);
            List<Transaction> transactions = new LinkedList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray transactionArray = jsonArray.getJSONArray(i);
                for (int j = 0; j < transactionArray.length(); j++) {
                    Transaction transaction = getTransactionFromJson(transactionArray.getJSONObject(j));
                    transactions.add(transaction);
                }
            }
            return transactions;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}