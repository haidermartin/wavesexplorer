package at.jku.se.wavesexplorer.utils;

import at.jku.se.wavesexplorer.model.Address;
import at.jku.se.wavesexplorer.model.Group;
import at.jku.se.wavesexplorer.model.Member;
import at.jku.se.wavesexplorer.service.AddressService;
import at.jku.se.wavesexplorer.service.GroupService;
import at.jku.se.wavesexplorer.service.MemberService;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileImporter {

    public static void fillMembersDbFromFile(AddressService addressService, GroupService groupService, MemberService memberService, MultipartFile file, Group group) {
        boolean colsDefined = false, leftSideAddress = false, mustUpdateGroup = false;
        try {
            InputStream inputStream = file.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = bufferedReader.readLine();
            System.out.println("START LINE");
            while (line != null) {
                System.out.println(line);
                String[] cols = line.split(";");
                if (cols.length == 2) {
                    if (colsDefined == false) {
                        if (addressService.exists(cols[0])) {
                            leftSideAddress = true;
                            colsDefined = true;
                        } else if (addressService.exists(cols[1])) {
                            leftSideAddress = false;
                            colsDefined = false;
                        }
                    }
                    if (colsDefined) {
                        String[] name = null;
                        Address address = null;
                        if (leftSideAddress) {
                            // Addresse finden
                            address = addressService.find(cols[0]);
                            name = cols[1].split(" ");
                            if (name == null) {
                                name = new String[]{ cols[1] };
                            }
                        } else {
                            // Addresse finden
                            address = addressService.find(cols[1]);
                            name = cols[0].split(" ");
                            if (name == null) {
                                name = new String[]{ cols[0] };
                            }
                        }
                        if (address != null) {
                            // Mitglied existiert für diese Addresse bereits -> nicht anlegen
                            Member byAddress = memberService.findByAddress(address);
                            if (byAddress == null) {
                                Member member = new Member();
                                member.setAddress(address);
                                if (name.length > 0) {
                                    member.setFirstName(name[0]);
                                }
                                if (name.length > 1) {
                                    member.setLastName(name[1]);
                                }
                                member.getGroups().add(group);
                                memberService.create(member);
                                group.getMembers().add(member);
                                mustUpdateGroup = true;
                            } else{
                                group.getMembers().add(byAddress);
                                groupService.update(group);
                            }
                        }
                    }
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mustUpdateGroup) {
            groupService.update(group);
        }
    }
}
