package at.jku.se.wavesexplorer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

@SpringBootApplication
@EnableScheduling
public class WavesexplorerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WavesexplorerApplication.class, args);
	}

	@Bean
	@ConfigurationProperties(prefix="app.datasource")
	public DataSource dataSource(){
		return DataSourceBuilder.create().build();
	}
}
