package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Group;
import at.jku.se.wavesexplorer.model.Member;
import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.pojo.StatisticPojo;
import at.jku.se.wavesexplorer.pojo.StatisticsGroupPojo;
import at.jku.se.wavesexplorer.service.BlockService;
import at.jku.se.wavesexplorer.service.GroupService;
import at.jku.se.wavesexplorer.service.StatisticsService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private BlockService blockService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private StatisticsService statisticsService;

    @Autowired
    private TransactionService transactionService;

    /*
        Default method
        @return: statistics view
    */
    @RequestMapping("")
    public String index(ModelMap modelMap) {

        return "statistics";
    }

    /*
        Statistics over whole database
        getStatistics is called from javascript in parallel with "index" method and loads the statistics-data back into javascript
        @param: int days
        @return: StatisticsPojo statistics of waves
    */
    @RequestMapping("/statistic")
    @ResponseBody
    public StatisticPojo getStatistics(ModelMap modelMap, @RequestParam(value="days", defaultValue="0") int days) {

        StatisticPojo statistics = new StatisticPojo();
        double avgBlocksize = 0.0;
        double avgTransactions = 0.0;
        double avgTransactionsBlock = 0.0;

        //get all months
        for(Object[] s : statisticsService.findDateRange()){
            statistics.getMonths().add(new DateFormatSymbols().getMonths()[(int)s[1]-1] + " " + s[0]);
        }

        statistics.setBlockSize(statisticsService.findAvgBlocksize());
        statistics.setTransactions(statisticsService.findTransactions());
        statistics.getTransactions().add(0,(long)0);
        List<Long> listCountBlock = statisticsService.findCountBlock();
        listCountBlock.add(0,(long)0);

        //get sum of all block sizes
        for(Double d : statistics.getBlockSize()){
            avgBlocksize += d;
        }

        //get sum of all transactions amount
        for(Long d : statistics.getTransactions()){
            avgTransactions += d;
        }

        statistics.setAvgBlocksize(Math.floor(avgBlocksize/statistics.getMonths().size()*100)/100);
        statistics.setAvgTransactions(avgTransactions);

        //check if group by was correct, so blocks and transactions are grouped by months
        if(listCountBlock.size() == statistics.getTransactions().size()){

            for(int i=0; i<listCountBlock.size(); i++){
                if((statistics.getTransactions().get(i) > 0) && (listCountBlock.get(i) > 0)){

                    statistics.getTransactionsBlock().add((double)(statistics.getTransactions().get(i)/listCountBlock.get(i)));
                    avgTransactionsBlock += (double)statistics.getTransactions().get(i)/listCountBlock.get(i);
                } else{
                    statistics.getTransactionsBlock().add(0.0);
                }
            }
        }

        statistics.setAvgTransactionsBlock(Math.floor(avgTransactionsBlock/listCountBlock.size()*100)/100);
        statistics.setTransactionsType(statisticsService.findCountType());

        return statistics;
    }

    /*
        Statistics group specific
        @param: int days
        @return: StatisticsGroupPojo statistics of group
    */
    @RequestMapping("/statistic/group")
    @ResponseBody
    public StatisticsGroupPojo getStatistics(ModelMap modelMap, @RequestParam(value="name") String groupName, @RequestParam(value="days", defaultValue="0") int days) {

        StatisticsGroupPojo statistics = new StatisticsGroupPojo();
        List<Transaction> transactions = new ArrayList<Transaction>();
        long transactionsPerMemberCounter = 0;
        long transactionsPerDateCounter = 0;
        long totalTransactionsCounter = 0;
        double balancePerDateSum = 0.0;
        String monthAndYear = "";

        Group group = groupService.findByName(groupName);

        if(group != null){
            if(group.getMembers() != null){

                //get all group transactions
                for(Member memberSender : group.getMembers()) {
                    for(Member memberRecipient : group.getMembers()){
                        transactions.addAll(transactionService.findBetweenSenderAndRecipient(memberSender.getAddress().getAddress(), memberRecipient.getAddress().getAddress()));
                    }
                }

                //get statistics by member
                for(Member member : group.getMembers()){
                    statistics.getMembers().add(member.getFirstName() + " " + member.getLastName());
                    statistics.getBalancePerMember().add(member.getAddress().getBalance()/100000000);

                    for(Transaction transaction : transactions){
                        if(transaction.getSenderAddress().getAddress().equals(member.getAddress().getAddress())){
                            transactionsPerMemberCounter++;
                        }
                        monthAndYear = (transaction.getTimestamp().getTime().getMonth()+1) + " " + (transaction.getTimestamp().getTime().getYear()+1900);

                        if(!statistics.getMonths().contains(monthAndYear)){
                            statistics.getMonths().add(monthAndYear);
                        }
                    }

                    statistics.getTransactionsPerMember().add(transactionsPerMemberCounter);
                    transactionsPerMemberCounter = 0;

                }

                for(String s : statistics.getMonths()){
                    for(Transaction t : transactions){
                        monthAndYear = (t.getTimestamp().getTime().getMonth()+1) + " " + (t.getTimestamp().getTime().getYear()+1900);

                        if(monthAndYear.equals(s)){
                            transactionsPerDateCounter++;
                            totalTransactionsCounter++;
                            balancePerDateSum += t.getAmount();
                        }
                    }
                    statistics.getTotalTransactionsPerDate().add(transactionsPerDateCounter);
                    statistics.getBalancePerDate().add(balancePerDateSum/100000000);
                    transactionsPerDateCounter = 0;
                    balancePerDateSum = 0.0;
                }
                statistics.setTotalTransactions(totalTransactionsCounter);
            }
        }
        return statistics;
    }
}