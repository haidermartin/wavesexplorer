package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    /*
       Default method
       @param: String transaction id
       @return: transaction view
   */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getTransactionById(Model model, @RequestParam("id") String id){
        Transaction transaction = transactionService.findByTransactionId(id);

        model.addAttribute("transaction", transaction);

        return "transaction";
    }
}