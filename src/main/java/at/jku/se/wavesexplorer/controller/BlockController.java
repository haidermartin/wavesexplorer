package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Block;
import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.service.BlockService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/block")
public class BlockController {

    @Autowired
    private BlockService blockService;

    @Autowired
    private TransactionService transactionService;

    /*
        Default method, gets block information and all transactions in the given block
        @param: long height
        @return: address view
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getBlockByHeight(Model model, @RequestParam("height") long height){
       Block block = blockService.findByHeight(height);

       //find maxHeight in database, to hide "next"-button in jsp if it is the last block
       long maxHeight = blockService.findMaxHeight();

       List<Transaction> transactions = transactionService.findAllByHeight(block.getHeight());

       model.addAttribute("block", block);
       model.addAttribute("maxHeight", maxHeight);
       model.addAttribute("transactions", transactions);
       model.addAttribute("transactionsSize", transactions.size());

       return "block";
    }
}