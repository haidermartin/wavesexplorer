package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Asset;
import at.jku.se.wavesexplorer.model.Group;
import at.jku.se.wavesexplorer.service.AssetService;
import at.jku.se.wavesexplorer.service.GroupService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/groups")
public class GroupsController {

    @Autowired
    AssetService assetService;

    @Autowired
    GroupService groupService;

    @Autowired
    TransactionService transactionService;

    /*
        Default method, gets groups information and meta data
        @return: groups view
    */
    @RequestMapping("")
    public String index(ModelMap modelMap) {

        List<Group> groups = groupService.findAll();
        List<Asset> assets = assetService.findAll();

        /*
        //This part is in comments, to improve performance and not load all data

        List<Transaction> transactions = new ArrayList<Transaction>();
        double totalAmount = 0.0;
        Calendar latestTransaction = null;

        for(Group group : groups){

            if(group.getMembers() != null){
                for(Member memberSender : group.getMembers()) {
                    for(Member memberRecipient : group.getMembers()){
                        transactions.addAll(transactionService.findBetweenSenderAndRecipient(memberSender.getAddress().getAddress(), memberRecipient.getAddress().getAddress()));
                    }
                }
            }

            if(transactions != null){
                if(transactions.size() > 0){
                    latestTransaction = transactions.get(0).getTimestamp();
                    for(Transaction transaction : transactions){
                        if(transaction.getAmount() != null){
                            totalAmount += transaction.getAmount();
                        }

                    if(transaction.getTimestamp().getTimeInMillis() > latestTransaction.getTimeInMillis()){
                        latestTransaction = transaction.getTimestamp();
                    }
                    }
                }
            }

            group.setTransactionVolume(totalAmount);
            group.setLatestTransactionDate(latestTransaction);

            transactions = new ArrayList<Transaction>();
            totalAmount = 0.0;
            latestTransaction = null;

        }*/

        modelMap.put("groups", groups);
        modelMap.put("assets", assets);

        return "groups";
    }

    /*
        Adds a new group
        @body: Group new group
        @return: groups view
    */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addGroup(@RequestBody Group groupToAdd, ModelMap modelMap){

        //set parent of the new group
        if(groupToAdd.getParentGroup().getName().equals("None")){
            groupToAdd.setParentGroup(null);
        } else{
            groupToAdd.setParentGroup(groupService.findByName(groupToAdd.getParentGroup().getName()));
        }

        groupToAdd.setTransactionVolume(0);
        groupToAdd.setLatestTransactionDate(null);

        groupService.add(groupToAdd);

        List<Group> groups = groupService.findAll();

        modelMap.put("groups", groups);

        return "groups";
    }
}