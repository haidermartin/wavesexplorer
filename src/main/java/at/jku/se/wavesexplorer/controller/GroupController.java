package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Asset;
import at.jku.se.wavesexplorer.model.Group;
import at.jku.se.wavesexplorer.model.Member;
import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.service.*;
import at.jku.se.wavesexplorer.utils.FileImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/group")
public class GroupController {

    @Autowired
    AddressService addressService;

    @Autowired
    AssetService assetService;

    @Autowired
    GroupService groupService;

    @Autowired
    MemberService memberService;

    @Autowired
    TransactionService transactionService;

    /*
        Default method, gets group information, all transactions, all members and statistics of the given group
        @param: String group name
        @return: group view
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(Model model, @RequestParam("name") String name) {

        List<Asset> assets = assetService.findAll();
        Group group = groupService.findByName(name);
        List<Transaction> transactionsBySender = new ArrayList<Transaction>();
        List<Transaction> validGroupTransactions = new ArrayList<Transaction>();
        double totalAmount = 0.0;

        if (group.getMembers() != null) {

            //get all transactions where the senderAddress is a member of this group
            for (Member memberSender : group.getMembers()) {
                transactionsBySender.addAll(transactionService.findBySenderAddress(memberSender.getAddress().getAddress()));
            }

            //check all transactions where the senderAddress is a member of this group, if the recipientAddress is also a member of this group, then add it
            for (Transaction t : transactionsBySender) {
                for (Member memberRecipient : group.getMembers()) {
                    if (memberRecipient.getAddress().getAddress().equals(t.getRecipientAddress().getAddress())) {
                        validGroupTransactions.add(t);
                        totalAmount += t.getAmount();
                    }
                }
            }
        }

        model.addAttribute("group", group);
        model.addAttribute("assets", assets);
        model.addAttribute("transactions", validGroupTransactions);
        model.addAttribute("totalAmount", totalAmount);

        return "group";
    }

    /*
        Update group
        @param: String old group name
        @body: Group group update
        @return: group view
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateGroup(@RequestParam("oldname") String oldname, @RequestBody Group groupToUpdate, ModelMap modelMap){

        Group group = groupService.findByName(oldname);

        group.setName(groupToUpdate.getName());
        group.setCurrency(groupToUpdate.getCurrency());

        groupService.update(group);

        return "group";
    }

    /*
       Add member to a group
       @body: Member new member
       @return: group view
    */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addMember(@RequestBody Member memberToAdd, ModelMap modelMap){
        String groupName = memberToAdd.getGroups().get(0).getName();
        Group group = groupService.findByName(groupName);

        Member member = memberService.findByAddress(addressService.findByAddress(memberToAdd.getAddress().getAddress()));

        if(member != null){
            //address is already taken by another member, do not add
        } else {
            //address is free, add member to member to group

            memberToAdd.setAddress(addressService.findByAddress(memberToAdd.getAddress().getAddress()));
            memberToAdd.getGroups().remove(0);
            memberToAdd.getGroups().add(group);

            group.getMembers().add(memberToAdd);

            memberService.add(memberToAdd);
            groupService.update(group);
        }

        return "group";
    }

    /*MEMBER CONTROLLING BELOW*/

    /*
       Update member
       @param: String oldaddress
       @body: Member update member
       @return: group view
    */
    @RequestMapping(value = "/update/member", method = RequestMethod.POST)
    public String updateMember(@RequestParam("oldaddress") String oldaddress, @RequestBody Member memberToUpdate, ModelMap modelMap){
        Member member = memberService.findByAddress(addressService.findByAddress(oldaddress));

        member.setFirstName(memberToUpdate.getFirstName());
        member.setLastName(memberToUpdate.getLastName());
        member.setAddress(memberToUpdate.getAddress());

        memberService.update(member);

        return "group";
    }

    /*
       Delete member
       @param: String group name
       @body: Member delete member
       @return: group view
    */
    @RequestMapping(value="/delete/member", method = RequestMethod.DELETE)
    public String deleteMemeber(@RequestParam("groupname") String groupname, @RequestBody Member memberToDelete, ModelMap modelMap){

        Member member = memberService.findByAddress(addressService.findByAddress(memberToDelete.getAddress().getAddress()));

        //remove member from group
        Group group = groupService.findByName(groupname);
        group.getMembers().remove(member);

        memberService.delete(member);
        return "group";
    }

    /*
       Import member
       @param: String group name, MultipartFile file to import
       @return: group view
    */
    @RequestMapping(value="/upload/member", method = RequestMethod.POST)
    @ResponseBody
    public String uploadMember(@RequestParam("group") String groupName, @RequestParam("file") MultipartFile multipartFile){
        FileImporter fileImporter = new FileImporter();

        fileImporter.fillMembersDbFromFile(addressService, groupService, memberService, multipartFile, groupService.findByName(groupName));

        return "group";
    }
}