package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Address;
import at.jku.se.wavesexplorer.model.Block;
import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.service.AddressService;
import at.jku.se.wavesexplorer.service.BlockService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("search")
public class SearchController {

    @Autowired
    AddressService addressService;

    @Autowired
    BlockService blockService;

    @Autowired
    TransactionService transactionService;

    /*
        Default search method
        First, check if searchstring is a valid address, then if it is a block height, then if it is a transaction id, else return to index
        @param: String search
        @return: RedirectView address or block or transaction
    */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public RedirectView search(Model model, @RequestParam("searchstring") String searchstring){

        Address address = addressService.findByAddress(searchstring);

        if(address != null){
            return new RedirectView("address?address="+searchstring);
        }

        Block block = blockService.findBySignature(searchstring);

        if(block != null){
            return new RedirectView("block?height="+blockService.findBySignature(searchstring).getHeight());
        }

        Transaction transaction = transactionService.findByTransactionId(searchstring);

        if(transaction != null){
            return new RedirectView("transaction?id="+searchstring);
        }

        return new RedirectView("");
    }
}