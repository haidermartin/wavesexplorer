package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Address;
import at.jku.se.wavesexplorer.model.Transaction;
import at.jku.se.wavesexplorer.service.AddressService;
import at.jku.se.wavesexplorer.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private TransactionService transactionService;

    /*
        Default method, gets address information and all transactions with the given address
        @param: String address
        @return: address view
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(Model model, @RequestParam("address") String addressParam){

        Address address = addressService.findByAddress(addressParam);

        List<Transaction> transactions = transactionService.findRecipientOrSenderByAddress(addressParam);

        model.addAttribute("address", address);
        model.addAttribute("transactions", transactions);

        return "address";
    }
}