package at.jku.se.wavesexplorer.controller;

import at.jku.se.wavesexplorer.model.Block;
import at.jku.se.wavesexplorer.service.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private BlockService blockService;

    /*
        Start method of the wavesexplorer, gets the last 50 blocks in database
        @return: index view
    */
    @RequestMapping("")
    public String index(ModelMap modelMap){
        List<Block> blocks = blockService.findTop50();

        modelMap.put("blocks", blocks);

        return "index";
    }
}