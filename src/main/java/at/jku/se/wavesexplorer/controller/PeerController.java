package at.jku.se.wavesexplorer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/peer")
public class PeerController {

    /*
        Default method
    */
    @RequestMapping("")
    public String index(){
        return "";
    }

}