package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.AliasRepository;
import at.jku.se.wavesexplorer.model.Alias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AliasService {

    @Autowired
    private AliasRepository aliasRepository;

    public void create(Alias alias){
        aliasRepository.save(alias);
    }

    public void delete(Alias alias){
        aliasRepository.delete(alias);
    }

    public void deleteAll(){
        aliasRepository.deleteAll();
    }

    public Alias findById(String address) {
        return aliasRepository.findById(address);
    }
}