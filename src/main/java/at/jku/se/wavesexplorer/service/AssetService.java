package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.AssetRepository;
import at.jku.se.wavesexplorer.model.Asset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssetService {

    @Autowired
    private AssetRepository assetRepository;

    public void create(Asset asset){
        assetRepository.save(asset);
    }

    public void delete(Asset asset){
        assetRepository.delete(asset);
    }

    public void deleteAll(){
        assetRepository.deleteAll();
    }

    public Asset find(String assetId) {
        return assetRepository.findOne(assetId);
    }

    public long count() {
        return assetRepository.count();
    }

    public boolean exists(String assetId) {
        return assetRepository.exists(assetId);
    }

    public List<Asset> findAll() {
        return (List)assetRepository.findAll();
    }
}