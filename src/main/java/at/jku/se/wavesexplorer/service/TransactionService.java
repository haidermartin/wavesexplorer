package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.TransactionRepository;
import at.jku.se.wavesexplorer.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TransactionService {

    private Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private TransactionRepository transactionRepository;

    public void create(Transaction transaction){
        transactionRepository.save(transaction);
    }

    public void delete(Transaction transaction){
        transactionRepository.delete(transaction);
    }

    public void deleteAll(){
        transactionRepository.deleteAll();
    }

    public List<Transaction> findAllByHeight(long height) {
        return transactionRepository.findAllByHeight(height);
    }

    public Transaction findById(long id) {
        return transactionRepository.findById(id);
    }

    public List<Transaction> findRecipientOrSenderByAddress(String addressParam) {
        return transactionRepository.findRecipientOrSenderByAddress(addressParam);
    }

    public Transaction findByTransactionId(String searchstring) {
        return transactionRepository.findByTransactionId(searchstring);
    }

    public List<Transaction> findBetweenSenderAndRecipient(String senderAddress, String recipientAddress) {
        return transactionRepository.findBetweenSenderAndRecipient(senderAddress, recipientAddress);
    }

    public Collection<? extends Transaction> findBySenderAddress(String address) {
        return transactionRepository.findBySenderAddress(address);
    }
}