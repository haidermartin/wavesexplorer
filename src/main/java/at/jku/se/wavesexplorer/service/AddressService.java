package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.AddressRepository;
import at.jku.se.wavesexplorer.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public void create(Address address){
        addressRepository.save(address);
    }

    public void delete(Address address){
        addressRepository.delete(address);
    }

    public void deleteAll(){
        addressRepository.deleteAll();
    }

    public Address find(String address) {
        return addressRepository.findOne(address);
    }

    public long count() {
        return addressRepository.count();
    }

    public boolean exists(String address) {
        return addressRepository.exists(address);
    }

    public Address findByAddress(String address) {
        return addressRepository.findByAddress(address);
    }
}