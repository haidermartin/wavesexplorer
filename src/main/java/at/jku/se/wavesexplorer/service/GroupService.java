package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.GroupRepository;
import at.jku.se.wavesexplorer.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    public void create(Group group){
        groupRepository.save(group);
    }

    public void delete(Group group){
        groupRepository.delete(group);
    }

    public void deleteAll(){
        groupRepository.deleteAll();
    }

    public Group find(Long groupId) {
        return groupRepository.findOne(groupId);
    }

    public long count() {
        return groupRepository.count();
    }

    public boolean exists(Long groupId) {
        return groupRepository.exists(groupId);
    }

    public void add(Group groupToAdd) {
        groupRepository.save(groupToAdd);
    }

    public List<Group> findAll() {
        return (List)groupRepository.findAll();
    }

    public Group findByName(String name) {
        return groupRepository.findByName(name);
    }

    public void update(Group group){
        groupRepository.save(group);
    }
}