package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.MemberRepository;
import at.jku.se.wavesexplorer.model.Address;
import at.jku.se.wavesexplorer.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;

    public void create(Member member){
        memberRepository.save(member);
    }

    public void delete(Member member){
        memberRepository.delete(member);
    }

    public void deleteAll(){
        memberRepository.deleteAll();
    }

    public Member find(Long memberId) {
        return memberRepository.findOne(memberId);
    }

    public long count() {
        return memberRepository.count();
    }

    public boolean exists(Long memberId) {
        return memberRepository.exists(memberId);
    }

    public void add(Member memberToAdd) {
        memberRepository.save(memberToAdd);
    }

    public Member findByAddress(Address oldaddress) {
        return memberRepository.findByAddress(oldaddress);
    }

    public void update(Member member){
        memberRepository.save(member);
    }
}