package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.StatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticsService {

    @Autowired
    private StatisticsRepository statisticsRepository;

    public List<Object[]> findDateRange() {
        return statisticsRepository.findDateRange();
    }

    public List<Double> findAvgBlocksize(){
        return statisticsRepository.findAvgBlocksize();
    }

    public List<Long> findTransactions() {
        return statisticsRepository.findTransactions();
    }

    public List<Long> findCountBlock(){
        return statisticsRepository.findCountBlock();
    }

    public List<Long> findCountType(){
        return statisticsRepository.findCountType();
    }
}