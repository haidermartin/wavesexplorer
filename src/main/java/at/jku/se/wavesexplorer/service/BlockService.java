package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.BlockRepository;
import at.jku.se.wavesexplorer.model.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class BlockService {

    private Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private BlockRepository blockRepository;

    public void create(Block block){
        blockRepository.save(block);
    }

    public void delete(Block block){
        blockRepository.delete(block);
    }

    public void deleteAll(){
        blockRepository.deleteAll();
    }

    public long findMaxHeight() {
        return blockRepository.findMaxHeight();
    }

    public List<Block> findAll() {
        return (List)blockRepository.findAll();
    }

    public List<Block> findTop50(){
        return blockRepository.findTop50();
    }

    public Block findByHeight(long height) {
        return blockRepository.findByHeight(height);
    }

    public Block findBySignature(String searchstring) {
        return blockRepository.findBySignature(searchstring);
    }
}