package at.jku.se.wavesexplorer.service;

import at.jku.se.wavesexplorer.dao.PeerRepository;
import at.jku.se.wavesexplorer.model.Peer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeerService {

    @Autowired
    private PeerRepository peerRepository;

    public void create(Peer peer){
        peerRepository.save(peer);
    }

    public void delete(Peer peer){
        peerRepository.delete(peer);
    }

    public void deleteAll(){
        peerRepository.deleteAll();
    }

    public long count() {
        return peerRepository.count();
    }

    public boolean exists(Long id) {
        return peerRepository.exists(id);
    }
}