package at.jku.se.wavesexplorer.model;

import javax.persistence.*;

@Entity
@Table(name="alias")
public class Alias {

    @Id
    @Column(name="id")
    private String id;

    @ManyToOne(cascade = {})
    @JoinColumn(name = "address")
    private Address address;

    public Alias() { }

    public Alias(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alias alias = (Alias) o;

        if (id != null ? !id.equals(alias.id) : alias.id != null) return false;
        return address != null ? address.equals(alias.address) : alias.address == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}