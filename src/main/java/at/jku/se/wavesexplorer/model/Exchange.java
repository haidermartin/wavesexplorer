package at.jku.se.wavesexplorer.model;

public class Exchange extends Model {
    private Double price;
    private Long amount;
    private Double buyMatcherFee;
    private Double sellMatcherFee;
    private Order order1;
    private Order order2;

    public Exchange() {
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getBuyMatcherFee() {
        return buyMatcherFee;
    }

    public void setBuyMatcherFee(Double buyMatcherFee) {
        this.buyMatcherFee = buyMatcherFee;
    }

    public Double getSellMatcherFee() {
        return sellMatcherFee;
    }

    public void setSellMatcherFee(Double sellMatcherFee) {
        this.sellMatcherFee = sellMatcherFee;
    }

    public Order getOrder1() {
        return order1;
    }

    public void setOrder1(Order order1) {
        this.order1 = order1;
    }

    public Order getOrder2() {
        return order2;
    }

    public void setOrder2(Order order2) {
        this.order2 = order2;
    }
}
