package at.jku.se.wavesexplorer.model;

import javax.persistence.*;

@Entity
@Table(name="Asset")
public class Asset {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="name")
    private String name;

    @Lob
    @Column(name="description")
    private String description;

    @Column(name="issuer")
    private String issuer;

    @Column(name="balance")
    private Double balance;

    @Column(name = "reissuable")
    private Boolean reissuable;

    @Column(name = "quantity")
    private Double quantity;

    @Column(name="decimals")
    private Integer decimals;

    @ManyToOne(cascade = {})
    @JoinColumn(name="address")
    public Address address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Double getBalance() {
        return balance;
    }

    public Boolean getReissuable() {
        return reissuable;
    }

    public void setReissuable(Boolean reissuable) {
        this.reissuable = reissuable;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getDecimals() {
        return decimals;
    }

    public void setDecimals(Integer decimals) {
        this.decimals = decimals;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Asset asset = (Asset) o;

        if (id != null ? !id.equals(asset.id) : asset.id != null) return false;
        if (name != null ? !name.equals(asset.name) : asset.name != null) return false;
        if (description != null ? !description.equals(asset.description) : asset.description != null) return false;
        if (issuer != null ? !issuer.equals(asset.issuer) : asset.issuer != null) return false;
        if (balance != null ? !balance.equals(asset.balance) : asset.balance != null) return false;
        return reissuable != null ? reissuable.equals(asset.reissuable) : asset.reissuable == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (issuer != null ? issuer.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        result = 31 * result + (reissuable != null ? reissuable.hashCode() : 0);
        return result;
    }
}
