package at.jku.se.wavesexplorer.model;

import java.util.Calendar;

public class Order extends Model {
    private String orderId;
    private String signature;
    private String senderPublicKey;
    private String matcherPublicKey;
    private String amountAsset;
    private String priceAsset;
    private String orderType;
    private Double price;
    private Double amount;
    private Calendar timestamp;
    private Calendar expiration;
    private Double matcherFee;

    public Order() {

    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSenderPublicKey() {
        return senderPublicKey;
    }

    public void setSenderPublicKey(String senderPublicKey) {
        this.senderPublicKey = senderPublicKey;
    }

    public String getMatcherPublicKey() {
        return matcherPublicKey;
    }

    public void setMatcherPublicKey(String matcherPublicKey) {
        this.matcherPublicKey = matcherPublicKey;
    }

    public String getAmountAsset() {
        return amountAsset;
    }

    public void setAmountAsset(String amountAsset) {
        this.amountAsset = amountAsset;
    }

    public String getPriceAsset() {
        return priceAsset;
    }

    public void setPriceAsset(String priceAsset) {
        this.priceAsset = priceAsset;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public Calendar getExpiration() {
        return expiration;
    }

    public void setExpiration(Calendar expiration) {
        this.expiration = expiration;
    }

    public Double getMatcherFee() {
        return matcherFee;
    }

    public void setMatcherFee(Double matcherFee) {
        this.matcherFee = matcherFee;
    }
}
