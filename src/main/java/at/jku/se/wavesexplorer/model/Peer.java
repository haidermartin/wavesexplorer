package at.jku.se.wavesexplorer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="peer")
public class Peer extends Model {

    @Column(name="address")
    private String address;

    @Column(name="declaredaddress")
    private String declaredAddress;

    @Column(name="priavtename")
    private String privateName;

    @Column(name="peenounce")
    private Integer peerNounce;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeclaredAddress() {
        return declaredAddress;
    }

    public void setDeclaredAddress(String declaredAddress) {
        this.declaredAddress = declaredAddress;
    }

    public String getPrivateName() {
        return privateName;
    }

    public void setPrivateName(String privateName) {
        this.privateName = privateName;
    }

    public Integer getPeerNounce() {
        return peerNounce;
    }

    public void setPeerNounce(Integer peerNounce) {
        this.peerNounce = peerNounce;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Peer peer = (Peer) o;

        if (peerNounce != peer.peerNounce) return false;
        if (address != null ? !address.equals(peer.address) : peer.address != null) return false;
        if (declaredAddress != null ? !declaredAddress.equals(peer.declaredAddress) : peer.declaredAddress != null)
            return false;
        return privateName != null ? privateName.equals(peer.privateName) : peer.privateName == null;
    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (declaredAddress != null ? declaredAddress.hashCode() : 0);
        result = 31 * result + (privateName != null ? privateName.hashCode() : 0);
        result = 31 * result + (peerNounce != null ? peerNounce.hashCode() : 0);
        return result;
    }
}
