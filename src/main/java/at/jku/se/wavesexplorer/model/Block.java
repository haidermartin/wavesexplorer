package at.jku.se.wavesexplorer.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="block")
public class Block extends Model {

    @Column(name="version")
    private Double version;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="timestamp")
    private Calendar timestamp;

    @Column(name="height")
    private long height;

    @Column(name="signature")
    private String signature;

    @Column(name="fee")
    private Double fee;

    @Column(name="blocksize")
    private int blocksize;

    //previous block
    @OneToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name="reference")
    private Block reference;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "block")
    private List<Transaction> transactions;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name="generator")
    private Address generator;

    public Block() {
        transactions = new LinkedList<>();
    }

    public void addTransaction(Transaction transaction) {
        if (this.transactions == null) {
            this.transactions = new LinkedList<>();
        }
        if (transaction == null) {
            return;
        }
        transaction.setBlock(this);
        this.transactions.add(transaction);
    }

    public void addTransactions(List<Transaction> transactions) {
        for (Transaction t : transactions) {
            addTransaction(t);
        }
    }

    public Address getGenerator() {
        return generator;
    }

    public void setGenerator(Address generator) {
        this.generator = generator;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public int getBlocksize() {
        return blocksize;
    }

    public void setBlocksize(int blocksize) {
        this.blocksize = blocksize;
    }

    public Block getReference() {
        return reference;
    }

    public void setReference(Block reference) {
        this.reference = reference;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        if (height != block.height) return false;
        return signature != null ? signature.equals(block.signature) : block.signature == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (height ^ (height >>> 32));
        result = 31 * result + (signature != null ? signature.hashCode() : 0);
        return result;
    }
}