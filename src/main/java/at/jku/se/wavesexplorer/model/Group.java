package at.jku.se.wavesexplorer.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="group_table")
public class Group extends Model {

    @Column(name = "name")
    private String name;

    @Column(name = "currency")
    private String currency;

    @Column(name = "latestTransactionDate")
    private Calendar latestTransactionDate;

    @Column(name = "transactionVolume")
    private double transactionVolume;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentGroup")
    private List<Group> subGroup;

    @ManyToOne(cascade = { })
    @JoinColumn(name="parentGroup")
    private Group parentGroup;

    @ManyToMany(cascade = { })
    @JoinTable(name = "GroupMember",
            joinColumns = { @JoinColumn(name = "group_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn( name = "member_id", referencedColumnName = "id")})
    private List<Member> members;

    public Group() {
        subGroup = new LinkedList<>();
        members = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<Group> getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(List<Group> subGroup) {
        this.subGroup = subGroup;
    }

    public Group getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(Group parentGroup) {
        this.parentGroup = parentGroup;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Calendar getLatestTransactionDate() {
        return latestTransactionDate;
    }

    public void setLatestTransactionDate(Calendar latestTransactionDate) {
        this.latestTransactionDate = latestTransactionDate;
    }

    public double getTransactionVolume() {
        return transactionVolume;
    }

    public void setTransactionVolume(double transactionVolume) {
        this.transactionVolume = transactionVolume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (name != null ? !name.equals(group.name) : group.name != null) return false;
        return currency != null ? currency.equals(group.currency) : group.currency == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        return result;
    }
}
