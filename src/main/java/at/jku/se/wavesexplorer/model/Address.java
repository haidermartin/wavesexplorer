package at.jku.se.wavesexplorer.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="address")
public class Address {

    @Id
    @Column(name="address")
    private String address;

    @Column(name="balance")
    private Double balance;

    @Column(name="confirmations")
    private Integer confirmations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "generator")
    private List<Block> blocks;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "senderAddress")
    private List<Transaction> senderTransactions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipientAddress")
    private List<Transaction> recipientTransactions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
    private Set<Alias> alias;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
    private Set<Asset> assets;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "address")
    private Member member;

    public Address(){
        this.senderTransactions = new LinkedList<>();
        this.recipientTransactions = new LinkedList<>();
        this.assets = new HashSet<>();
        this.alias = new HashSet<>();
    }

    public Address(String address) {
        this();
        this.address = address;
    }

    public void addTransactionSender(Transaction transaction) {
        if (this.senderTransactions == null) {
            this.senderTransactions = new LinkedList<>();
        }
        if (transaction == null) {
            return;
        }
        transaction.setSenderAddress(this);
    }

    public void addTransactionRecipient(Transaction transaction) {
        if (this.recipientTransactions == null) {
            this.recipientTransactions = new LinkedList<>();
        }
        if (transaction == null) {
            return;
        }
        transaction.setRecipientAddress(this);
    }

    public void addAsset(Asset asset) {
        if (this.assets == null) {
            this.assets = new HashSet<>();
        }
        if (asset == null) {
            return;
        }
        asset.setAddress(this);
        this.assets.add(asset);
    }

    public void addBlock(Block b) {
        if (this.blocks == null) {
            this.blocks = new LinkedList<>();
        }
        if (b == null) {
            return;
        }
        b.setGenerator(this);
        this.blocks.add(b);
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(Integer confirmations) {
        this.confirmations = confirmations;
    }

    public List<Transaction> getSenderTransactions() {
        return senderTransactions;
    }

    public void setSenderTransactions(List<Transaction> senderTransactions) {
        this.senderTransactions = senderTransactions;
    }

    public List<Transaction> getRecipientTransactions() {
        return recipientTransactions;
    }

    public void setRecipientTransactions(List<Transaction> recipientTransactions) {
        this.recipientTransactions = recipientTransactions;
    }

    public Set<Alias> getAlias() {
        return alias;
    }

    public void setAlias(Set<Alias> alias) {
        this.alias = alias;
    }

    public void addAlias(Alias alias) {
        this.alias.add(alias);
        alias.setAddress(this);
    }

    public Set<Asset> getAssets() {
        return assets;
    }

    public void setAssets(Set<Asset> assets) {
        this.assets = assets;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        return address != null ? address.equals(address1.address) : address1.address == null;
    }

    @Override
    public int hashCode() {
        return address != null ? address.hashCode() : 0;
    }
}
