package at.jku.se.wavesexplorer.model;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "transaction")
public class Transaction extends Model {

    @Column(name="transactionId")
    private String transactionId;

    @Column(name="signature")
    private String signature;

   /* different transaction types
   1:  Genesis transaction

   2:  Payment transaction

   3:  Issue transaction

   4:  Transfer transaction

   5:  Reissue transaction

   6:  Burn transaction

   7:  Exchange transaction

   8:  Lease transaction

   9:  Lease cancel transaction

   10: Create alias transaction

   11: Make asset name unique transaction
   */
    @Column(name="type")
    private Integer type;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="timestamp")
    private Calendar timestamp;

    @Column(name="senderpublickey")
    private String senderPublicKey;

    @Column(name="amount")
    private Double amount;

    @Column(name="fee")
    private Double fee;

    @ManyToOne(cascade = { }, fetch = FetchType.LAZY)
    @JoinColumn(name="recipientAddress")
    private Address recipientAddress;

    @ManyToOne(cascade = { }, fetch = FetchType.LAZY)
    @JoinColumn(name="senderAddress")
    private Address senderAddress;

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name="block")
    private Block block;

    /** In case of AliasTransaction **/
    @OneToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name="alias")
    private Alias alias;

    /** In case of AssetTransaction **/
    @OneToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name="asset")
    private Asset asset;

    /** In case of lease transaction its the transactionId of the LeaseTransaction**/
    @Column(name="leaseid")
    private String leaseid;

    public Transaction() { }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public String getSenderPublicKey() {
        return senderPublicKey;
    }

    public void setSenderPublicKey(String senderPublicKey) {
        this.senderPublicKey = senderPublicKey;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Address getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(Address recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public Address getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(Address senderAddress) {
        this.senderAddress = senderAddress;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public Alias getAlias() {
        return alias;
    }

    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public String getLeaseid() {
        return leaseid;
    }

    public void setLeaseid(String leaseid) {
        this.leaseid = leaseid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (transactionId != null ? !transactionId.equals(that.transactionId) : that.transactionId != null)
            return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (senderPublicKey != null ? !senderPublicKey.equals(that.senderPublicKey) : that.senderPublicKey != null)
            return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (fee != null ? !fee.equals(that.fee) : that.fee != null) return false;
        return leaseid != null ? leaseid.equals(that.leaseid) : that.leaseid == null;
    }

    @Override
    public int hashCode() {
        int result = transactionId != null ? transactionId.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (senderPublicKey != null ? senderPublicKey.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (fee != null ? fee.hashCode() : 0);
        result = 31 * result + (leaseid != null ? leaseid.hashCode() : 0);
        return result;
    }
}