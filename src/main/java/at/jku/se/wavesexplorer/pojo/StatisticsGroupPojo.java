package at.jku.se.wavesexplorer.pojo;

import java.util.ArrayList;
import java.util.List;

public class StatisticsGroupPojo {

    private List<String> months;

    private List<Long> transactions;

    private List<String> members;

    private List<Long> transactionsPerMember;

    private List<Double> balancePerMember;

    private List<Double> balancePerDate;

    private List<Long> totalTransactionsPerDate;

    private long totalTransactions;

    public StatisticsGroupPojo(){
        months = new ArrayList<String>();
        members = new ArrayList<String>();
        transactionsPerMember = new ArrayList<Long>();
        balancePerMember = new ArrayList<Double>();
        totalTransactionsPerDate = new ArrayList<Long>();
        balancePerDate = new ArrayList<Double>();
    }

    public List<String> getMonths() {
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public List<Long> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Long> transactions) {
        this.transactions = transactions;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public List<Long> getTransactionsPerMember() {
        return transactionsPerMember;
    }

    public void setTransactionsPerMember(List<Long> transactionsPerMember) {
        this.transactionsPerMember = transactionsPerMember;
    }

    public List<Double> getBalancePerMember() {
        return balancePerMember;
    }

    public void setBalancePerMember(List<Double> balancePerMember) {
        this.balancePerMember = balancePerMember;
    }

    public List<Long> getTotalTransactionsPerDate() {
        return totalTransactionsPerDate;
    }

    public void setTotalTransactionsPerDate(List<Long> totalTransactionsPerDate) {
        this.totalTransactionsPerDate = totalTransactionsPerDate;
    }

    public long getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(long totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public List<Double> getBalancePerDate() {
        return balancePerDate;
    }

    public void setBalancePerDate(List<Double> balancePerDate) {
        this.balancePerDate = balancePerDate;
    }
}