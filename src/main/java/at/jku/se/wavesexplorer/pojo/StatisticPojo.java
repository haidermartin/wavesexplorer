package at.jku.se.wavesexplorer.pojo;

import java.util.ArrayList;
import java.util.List;

public class StatisticPojo {

    private List<String> months;

    private List<Double> blockSize;

    private List<Long> transactions;

    private List<Double> transactionsBlock;

    private List<Long> transactionsType;

    private double avgBlocksize = 0.0;

    private double avgTransactions = 0.0;

    private double avgTransactionsBlock = 0.0;

    public StatisticPojo(){
        months = new ArrayList<String>();
        blockSize = new ArrayList<Double>();
        transactionsBlock = new ArrayList<Double>();
    }

    public List<String> getMonths() {
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public List<Double> getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(List<Double> blockSize) {
        this.blockSize = blockSize;
    }

    public double getAvgBlocksize() {
        return avgBlocksize;
    }

    public void setAvgBlocksize(double avgBlocksize) {
        this.avgBlocksize = avgBlocksize;
    }

    public List<Long> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Long> transactions) {
        this.transactions = transactions;
    }

    public double getAvgTransactions() {
        return avgTransactions;
    }

    public void setAvgTransactions(double avgTransactions) {
        this.avgTransactions = avgTransactions;
    }

    public List<Double> getTransactionsBlock() {
        return transactionsBlock;
    }

    public void setTransactionsBlock(List<Double> transactionsBlock) {
        this.transactionsBlock = transactionsBlock;
    }

    public double getAvgTransactionsBlock() {
        return avgTransactionsBlock;
    }

    public void setAvgTransactionsBlock(double avgTransactionsBlock) {
        this.avgTransactionsBlock = avgTransactionsBlock;
    }

    public List<Long> getTransactionsType() {
        return transactionsType;
    }

    public void setTransactionsType(List<Long> transactionsType) {
        this.transactionsType = transactionsType;
    }
}