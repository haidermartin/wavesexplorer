package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Peer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly=true)
public interface StatisticsRepository extends CrudRepository<Peer, Long>{

    @Query("select distinct extract(year from b.timestamp), extract(month from b.timestamp) from Block b")
    List<Object[]> findDateRange();

    @Query("select avg(b.blocksize) from Block b Group BY extract(year from b.timestamp), extract(month from b.timestamp)")
    List<Double> findAvgBlocksize();

    @Query("select count(*) from Transaction t Group BY extract(year from t.timestamp), extract(month from t.timestamp)")
    List<Long> findTransactions();

    @Query("select count(distinct t.block) from Transaction t Group BY extract(year from t.timestamp), extract(month from t.timestamp)")
    List<Long> findCountBlock();

    @Query("select count(*) from Transaction t Group BY t.type")
    List<Long> findCountType();
}
