package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Asset;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface AssetRepository extends CrudRepository<Asset, String>{

}
