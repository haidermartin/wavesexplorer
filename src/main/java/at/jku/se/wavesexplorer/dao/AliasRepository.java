package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Alias;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface AliasRepository extends CrudRepository<Alias, Long>{

    Alias findById(String address);
}
