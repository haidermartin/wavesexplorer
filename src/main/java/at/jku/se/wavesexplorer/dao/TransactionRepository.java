package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Transactional(readOnly=true)
public interface TransactionRepository extends CrudRepository<Transaction, Long>{

    @Query("select t from Transaction t where t.block.height = ?1")
    List<Transaction> findAllByHeight(long blockHeight);

    Transaction findById(long id);

    @Query("select t from Transaction t where t.recipientAddress.address like ?1 or t.senderAddress.address like ?1 order by timestamp desc")
    List<Transaction> findRecipientOrSenderByAddress(String addressParam);

    Transaction findByTransactionId(String searchstring);

    @Query("select t from Transaction t where t.senderAddress.address like ?1 and t.recipientAddress.address like ?2 order by timestamp desc")
    List<Transaction> findBetweenSenderAndRecipient(String senderAddress, String recipientAddress);

    @Query("select t from Transaction t where t.senderAddress.address like ?1")
    Collection<? extends Transaction> findBySenderAddress(String address);
}