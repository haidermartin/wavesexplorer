package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Peer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface PeerRepository extends CrudRepository<Peer, Long>{
    
}
