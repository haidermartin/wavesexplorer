package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Block;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

@Transactional(readOnly=true)
public interface BlockRepository extends CrudRepository<Block, Long>{

    Block findByHeight(long height);

    Block findBySignature(String searchstring);

    @Query("select MAX(height) from Block")
    long findMaxHeight();

    @Query("select b from Block b where b.height > :#{#startHeight} and b.height <=:#{#lastHeight} ORDER BY height ASC")
    List<Block> findFromTo(@Param("startHeight") long startHeight, @Param("lastHeight") long lastHeight);

    @Query("select b from Block b where b.height > (select max(height)-50 from Block) and b.height <=(select max(height) from Block) ORDER BY height DESC")
    List<Block> findTop50();

    @Query("select avg(b.blocksize) from Block b where b.timestamp > ?1 and b.timestamp < ?2")
    Long findAvgByMonthAndYear(Calendar firstDate, Calendar lastDate);
}
