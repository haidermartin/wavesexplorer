package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface AddressRepository extends CrudRepository<Address, String>{

    Address findByAddress(String address);
}
