package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface GroupRepository extends CrudRepository<Group, Long>{

    Group findByName(String name);
}
