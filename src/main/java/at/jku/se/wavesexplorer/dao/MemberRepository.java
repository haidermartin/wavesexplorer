package at.jku.se.wavesexplorer.dao;

import at.jku.se.wavesexplorer.model.Address;
import at.jku.se.wavesexplorer.model.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=true)
public interface MemberRepository extends CrudRepository<Member, Long>{

    Member findByAddress(Address oldaddress);
}
