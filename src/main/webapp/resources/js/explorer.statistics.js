var statisticsPojo = "";

$(document).ready(function() {

    $("#stat-period").change(function() {
        getStatistic();
    });

    getStatistic();
});

function getStatistic(){
    var days = $("#stat-period option:selected").val();

    jQuery.get("statistics/statistic?days="+days, function(data){
        statisticsPojo = data;
        $("#id-avgblocksize-span").text(statisticsPojo.avgBlocksize.toFixed(2) + " bytes");
        $("#id-avgtransactions-span").text(statisticsPojo.avgTransactions);
        $("#id-avgtransactionsblock-span").text(statisticsPojo.avgTransactionsBlock.toFixed(2));
        generateStatistics();
    });
};


function generateStatistics(){

// Total Transactions
    var s1 = document.getElementById("statsTotalTransactions").getContext('2d');
    var statsTotalTransactions = new Chart(s1, {
        type: 'line',
        data: {
            //labels: ["May 16", "Jun 16", "Jul 16", "Aug 16", "Sep 16", "Oct 16", "Nov 16", "Dec 16", "Jan 17", "Feb 17", "Mar 17", "Apr 17", "May 17", "Jun 17", "Jul 17", "Aug 17", "Sep 17", "Oct 17", "Nov 17", "Dec 17", "Jan 18"],
            labels: statisticsPojo.months,
            datasets: [{
                label: 'Total Transactions',
                data: statisticsPojo.transactions,
                //data: [1000, 1500, 2100, 2700, 3000, 4000, 4900, 6000, 7100, 8400, 10000, 13000, 20000, 50000, 90000, 300000, 1000000, 5000000, 6000000, 7000000, 8000000],
                backgroundColor: 'rgba(123, 209, 106, 0.1)',
                borderColor: 'rgba(123, 209, 106, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });


// Avg Block Size
    var s2 = document.getElementById("statsAvgBlockSize").getContext('2d');
    var statsAvgBlockSize = new Chart(s2, {
        type: 'line',
        data: {
            labels: statisticsPojo.months,
            //labels: ["May 16", "Jun 16", "Jul 16", "Aug 16", "Sep 16", "Oct 16", "Nov 16", "Dec 16", "Jan 17", "Feb 17", "Mar 17", "Apr 17", "May 17", "Jun 17", "Jul 17", "Aug 17", "Sep 17", "Oct 17", "Nov 17", "Dec 17", "Jan 18"],
            datasets: [{
                label: 'Avg. Blocksize',
                data: statisticsPojo.blockSize,
                //data: [0.1, 0.4, 0.3, 0.5, 0.5, 0.6, 0.6, 0.8, 0.6, 0.7, 0.8, 0.8, 0.9, 0.7, 0.5, 0.6, 0.7, 0.75, 0.9, 1, 1.3],
                backgroundColor: 'rgba(123, 209, 106, 0.1)',
                borderColor: 'rgba(123, 209, 106, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

// Transaction Types
    var s3 = document.getElementById("statsTransactionTypes").getContext('2d');
    var statsTransactionTypes = new Chart(s3, {
        type: 'doughnut',
        data: {
            labels: ["Genesis", "Payment", "Issue", "Transfer", "Reissue", "Burn", "Exchange", "Lease", "Lease cancel", "Create alias", " Make asset name unique"],
            datasets: [{
                label: 'Avg. Blocksize',
                //data: [5, 5, 10, 60, 2, 2, 6, 1, 3, 3, 3],
                data: statisticsPojo.transactionsType,
                backgroundColor: ['rgba(193, 66, 66, 0.6)',
                    'rgba(191, 112, 63, 0.6)',
                    'rgba(191, 189, 63, 0.6)',
                    'rgba(131, 191, 63, 0.6)',
                    'rgba(63, 191, 74, 0.6)',
                    'rgba(63, 191, 150, 0.6)',
                    'rgba(63, 174, 191, 0.6)',
                    'rgba(63, 108, 191, 0.6)',
                    'rgba(65, 63, 191, 0.6)',
                    'rgba(110, 63, 191, 0.6)',
                    'rgba(191, 63, 187, 0.6)']
            }]
        },
        options: {
        }
    });

// Transactions per Block
    var s4 = document.getElementById("statsTransactionsPerBlock").getContext('2d');
    var statsTransactionsPerBlock = new Chart(s4, {
        type: 'line',
        data: {
            //labels: ["May 16", "Jun 16", "Jul 16", "Aug 16", "Sep 16", "Oct 16", "Nov 16", "Dec 16", "Jan 17", "Feb 17", "Mar 17", "Apr 17", "May 17", "Jun 17", "Jul 17", "Aug 17", "Sep 17", "Oct 17", "Nov 17", "Dec 17", "Jan 18"],
            labels: statisticsPojo.months,
            datasets: [{
                label: 'Transactions per Block',
                //data: [5, 50, 99, 30, 45, 20, 80, 90, 69, 27, 60, 80, 100, 90, 40, 60, 50, 95, 80, 50, 87],
                data: statisticsPojo.transactionsBlock,
                backgroundColor: 'rgba(123, 209, 106, 0.1)',
                borderColor: 'rgba(123, 209, 106, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
};