var statisticsPojo = "";

$(document).ready(function() {

    $("#stat-period").change(function() {
        getStatistic();
    });

    getStatistic();
});

function getStatistic(){
    var days = $("#stat-period option:selected").val();

    jQuery.get("statistics/statistic/group?name="+$("#group-header").text(), function(data){
        statisticsPojo = data;
        $("#id-totaltransactions-group-span").text(statisticsPojo.totalTransactions);
        /*$("#id-avgblocksize-span").text(statisticsPojo.avgBlocksize.toFixed(2) + " bytes");
        $("#id-avgtransactions-span").text(statisticsPojo.avgTransactions);
        $("#id-avgtransactionsblock-span").text(statisticsPojo.avgTransactionsBlock.toFixed(2));*/
        generateStatistics();
    });
};

function generateStatistics() {

// Transaction per Member
    var s1 = document.getElementById("groupTransactionsPerMember").getContext('2d');
    var groupTransactionsPerMember = new Chart(s1, {
        type: 'doughnut',
        data: {
            //labels: ["Lukas Bernert", "Martin Haider", "Lukas Eder", "Marco Peilsteiner"],
            labels: statisticsPojo.members,
            datasets: [{
                label: 'Transactions/Member',
                //data: [5, 5, 10, 3],
                data: statisticsPojo.transactionsPerMember,
                backgroundColor: ['rgba(193, 66, 66, 0.6)',
                    'rgba(191, 189, 63, 0.6)',
                    'rgba(63, 191, 74, 0.6)',
                    'rgba(63, 174, 191, 0.6)']
            }]
        },
        options: {}
    });

// Total Transactions
    var s2 = document.getElementById("groupTotalTransactions").getContext('2d');
    var groupTotalTransactions = new Chart(s2, {
        type: 'line',
        data: {
            //labels: ["Nov 17", "Dec 17", "Jan 18"],
            labels: statisticsPojo.months,
            datasets: [{
                label: 'Total Transactions',
                //data: [3, 5, 10],
                data: statisticsPojo.totalTransactionsPerDate,
                backgroundColor: 'rgba(123, 209, 106, 0.1)',
                borderColor: 'rgba(123, 209, 106, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

// Balance per Member
    var s3 = document.getElementById("groupBalancePerMember").getContext('2d');
    var groupBalancePerMember = new Chart(s3, {
        type: 'bar',
        data: {
            //labels: ["Lukas Bernert", "Martin Haider", "Lukas Eder", "Marco Peilsteiner"],
            labels: statisticsPojo.members,
            datasets: [{
                label: 'Balance',
                //data: [20, 22.4, 19, 20],
                data: statisticsPojo.balancePerMember,
                backgroundColor: ['rgba(193, 66, 66, 0.4)',
                    'rgba(191, 189, 63, 0.4)',
                    'rgba(63, 191, 74, 0.4)',
                    'rgba(63, 174, 191, 0.4)'],
                borderColor: ['rgba(193, 66, 66, 1)',
                    'rgba(191, 189, 63, 1)',
                    'rgba(63, 191, 74, 1)',
                    'rgba(63, 174, 191, 1)'],
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

// Group Balance
    var s4 = document.getElementById("groupBalance").getContext('2d');
    var groupBalance = new Chart(s4, {
        type: 'line',
        data: {
            //labels: ["Nov 17", "Dec 17", "Jan 18"],
            labels: statisticsPojo.months,
            datasets: [{
                label: 'Group Balance',
                //data: [30, 40, 50],
                data: statisticsPojo.balancePerDate,
                backgroundColor: 'rgba(123, 209, 106, 0.1)',
                borderColor: 'rgba(123, 209, 106, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
};