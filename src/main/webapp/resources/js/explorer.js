var memberUpdateOldAddress = "";
var memberDeleteAddress = "";
var files = [];

//file import
$(document)
    .on(
        "change",
        "#memberInputFile",
        function (event) {
            files = event.target.files;
        });

function processUpload() {
    var fileData = new FormData();
    fileData.append("file", files[0]);

    $.ajax({
            dataType: 'json',
            url: "group/upload/member?group="+$("#group-header").text(),
            data: fileData,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (result) {
                console.log("UPLOAD succeeded")
            },
            error: function (result) {
                console.log("UPLOAD failed");
            }
        });
};

$(document).ready(function () {


  /***
   *      _           _            _     ____  _            _
   *     | |         | |          | |   |  _ \| |          | |
   *     | |     __ _| |_ ___  ___| |_  | |_) | | ___   ___| | _____
   *     | |    / _` | __/ _ \/ __| __| |  _ <| |/ _ \ / __| |/ / __|
   *     | |___| (_| | ||  __/\__ \ |_  | |_) | | (_) | (__|   <\__ \
   *     |______\__,_|\__\___||___/\__| |____/|_|\___/ \___|_|\_\___/
   *
   *
   */
  var latestBlockTable = $('#latestBlocksTable').DataTable({
    dom: 'rt',
    columnDefs: [{
      targets: 2,
      //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        },
      width: "25%",
    }],
    order: [[0, "desc"]],
    ordering: false,
      pageLength: 50
  });


  /***
   *      ____  _            _      _______
   *     |  _ \| |          | |    |__   __|
   *     | |_) | | ___   ___| | __    | |_  __
   *     |  _ <| |/ _ \ / __| |/ /    | \ \/ /
   *     | |_) | | (_) | (__|   <     | |>  <
   *     |____/|_|\___/ \___|_|\_\    |_/_/\_\
   *
   *
   */
  var blockTransactionTable = $('#blockTransactionTable').DataTable({
    dom: 'rtp',
    columnDefs: [{
      targets: [0, 1, 2],
      //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        },
        pageLength: 50,
      orderable: false
    }],
    order: [[5, "desc"]]
  });

  // Adding the search field in every column's footer
  $('#blockTransactionTable tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control form-control-sm max-w-90" placeholder="Search ' + title + '" />');
  });

  $('#blockTransactionSearch').keyup(function () {
    blockTransactionTable.search($(this).val()).draw();
  });

  // Search function for each column
  blockTransactionTable.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
      if (that.search() !== this.value) {
        that
          .search(this.value)
          .draw();
      }
    });
  });


  /***
   *                  _     _                     _______
   *         /\      | |   | |                   |__   __|
   *        /  \   __| | __| |_ __ ___  ___ ___     | |_  __
   *       / /\ \ / _` |/ _` | '__/ _ \/ __/ __|    | \ \/ /
   *      / ____ \ (_| | (_| | | |  __/\__ \__ \    | |>  <
   *     /_/    \_\__,_|\__,_|_|  \___||___/___/    |_/_/\_\
   *
   *
   */
  var addressTransactionTable = $('#addressTransactionTable').DataTable({
    dom: 'rtp',
    columnDefs: [{
        targets: [0, 1, 3],
        //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        },
        pageLength: 10,
        width: "20%",
        orderable: false
        },
      {
        targets: 2,
        // the third column can contain either 1 or 0 as text. If 1 then show a green arrow otherwise show a red arrow
        render: function (data, type, row, meta) {
          if (data == 1) {
            return '<i class="la la-arrow-right green la-2x"></i>'
          }
          return '<i class="la la-arrow-right red la-2x"></i>'
        },
        orderable: false
      }
    ],
    order: [[6, "desc"]]
  });

  // Adding the search field in every column's footer
  $('#addressTransactionTable tfoot th').not('.arrow').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control form-control-sm max-w-90" placeholder="Search ' + title + '" />');
  });

  $('#addressTransactionSearch').keyup(function () {
    addressTransactionTable.search($(this).val()).draw();
  });

  // Search function for each column
  addressTransactionTable.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
      if (that.search() !== this.value) {
        that
          .search(this.value)
          .draw();
      }
    });
  });


  /***
   *       _____                         _______
   *      / ____|                       |__   __|
   *     | |  __ _ __ ___  _   _ _ __      | |_  __
   *     | | |_ | '__/ _ \| | | | '_ \     | \ \/ /
   *     | |__| | | | (_) | |_| | |_) |    | |>  <
   *      \_____|_|  \___/ \__,_| .__/     |_/_/\_\
   *                            | |
   *                            |_|
   */
  var groupTransactionTable = $('#groupTransactionTable').DataTable({
    dom: 'rtp',
    columnDefs: [{
        targets: [0, 1, 2],
        //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        },
        width: "17.5%"
      },
      {
        targets: 0,
        orderable: false
      },
      {
        targets: [3, 4, 5],
        "width": "10%"
      }
    ],
    order: [[6, "desc"]],
    autoWidth: false
  });

  $('#groupTransactionSearch').keyup(function () {
    groupTransactionTable.search($(this).val()).draw();
  });

  // Adding the search field in every column's footer
  $('#groupTransactionTable tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" class="form-control form-control-sm max-w-90" placeholder="Search ' + title + '" />');
  });

  // Search function for each column
  groupTransactionTable.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
      if (that.search() !== this.value) {
        that
          .search(this.value)
          .draw();
      }
    });
  });


  /***
   *       _____                         __  __                _
   *      / ____|                       |  \/  |              | |
   *     | |  __ _ __ ___  _   _ _ __   | \  / | ___ _ __ ___ | |__   ___ _ __ ___
   *     | | |_ | '__/ _ \| | | | '_ \  | |\/| |/ _ \ '_ ` _ \| '_ \ / _ \ '__/ __|
   *     | |__| | | | (_) | |_| | |_) | | |  | |  __/ | | | | | |_) |  __/ |  \__ \
   *      \_____|_|  \___/ \__,_| .__/  |_|  |_|\___|_| |_| |_|_.__/ \___|_|  |___/
   *                            | |
   *                            |_|
   */
  var groupMembersTable = $('#groupMembersTable').DataTable({
    autoWidth: false,
    dom: 'rtp',
    columnDefs: [{
        targets: 2,
        //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        },
        width: "25%",
    },
      {
        targets: 5,
        orderable: false,
        searchable: false
    }
    ],
    order: [[1, "asc"]],
  });

  $('#groupMemberSearch').keyup(function () {
    groupMembersTable.search($(this).val()).draw();
  });

  // Show/Hide the Edit/Delete Buttons on hover
  $('#groupMembersTable').
  on('mouseover', 'tr', function () {
    $(this).find('.memberActionIcon').removeClass('transparent-color');
  }).
  on('mouseout', 'tr', function () {
    $(this).find('.memberActionIcon').addClass('transparent-color');
  });

  // Open editMemberModal Box when the edit button is clicked
  $('.memberEdit').on('click', function () {
    var selectedRow = $(this).closest('tr');
    var firstname = groupMembersTable.row(selectedRow).data()[0];
    var lastname = groupMembersTable.row(selectedRow).data()[1];
    var address = groupMembersTable.row(selectedRow).data()[2];
    $("#editMemberLabel").html('Edit ' + firstname + ' ' + lastname);
    $("#editMemberFirstname").val(firstname);
    $("#editMemberLastname").val(lastname);
    $("#editMemberAddress").val(address);
    $('#editMemberModal').modal("show");
      memberUpdateOldAddress = address;

  });

  // Open deleteMemberModal Box when the delete button is clicked
  $('.memberDelete').on('click', function () {
    var selectedRow = $(this).closest('tr');
    var firstname = groupMembersTable.row(selectedRow).data()[0];
    var lastname = groupMembersTable.row(selectedRow).data()[1];
      memberDeleteAddress = groupMembersTable.row(selectedRow).data()[2];
    $("#deleteMemberLabel").html('Delete ' + firstname + ' ' + lastname);
    $("#deleteMemberName").html(firstname + ' ' + lastname);
    $('#deleteMemberModal').modal("show");
  });


  /***
   *       _____
   *      / ____|
   *     | |  __ _ __ ___  _   _ _ __  ___
   *     | | |_ | '__/ _ \| | | | '_ \/ __|
   *     | |__| | | | (_) | |_| | |_) \__ \
   *      \_____|_|  \___/ \__,_| .__/|___/
   *                            | |
   *                            |_|
   */
  var groupsTable = $('#groupsTable').DataTable({
    autoWidth: false,
    dom: 'rtp',
    columnDefs: [{
        targets: 0,
        //If an entry is longer than 40 Characters truncate it to 35 characters
        render: function (data, type, row, meta) {
            var begin = data.indexOf('>')+1;
            var end = data.lastIndexOf('<');
            var address = data.substring(begin, end);
            return type === 'display' && address.length > 16 ?
                data.substring(0, begin) + address.substring(0, 16) + '...' + data.substring(end) : data;
        }
    }
    ],
    rowGroup: {
      dataSrc: 0
    }
  });

  $('#groupsSearch').keyup(function () {
    groupsTable.search($(this).val()).draw();
  });
});


$("#btn-add-group").click(function(){

  var groupToAdd = {
      name:$("#input-group-name").val(),
      currency:$("#select-group-currency option:selected" ).text(),
      parentGroup:{
           name:$("#select-group-parent option:selected" ).text()
      }
  };

  $.ajax({
      type:'POST',
      url:'groups/add',
      contentType:'application/json; charset=utf-8',
      data: JSON.stringify(groupToAdd),
      success: function (data) {
          console.log(data);
      }
  });

  $("#addGroupModal").modal('toggle');
});

$("#btn-add-member").click(function(){

    var memberToAdd = {
        firstName:$("#input-member-firstname").val(),
        lastName:$("#input-member-lastname").val(),
        address:$("#input-member-address").val(),
        groups: [{
          name:$("#group-header").text()
        }]
    };
    console.log(memberToAdd);
    $.ajax({
        type:'POST',
        url:'group/add',
        contentType:'application/json; charset=utf-8',
        data: JSON.stringify(memberToAdd),
        success: function (data) {
            console.log(data);
        }
    });

    $("#addMemberModal").modal('toggle');
});

$("#btn-search").click(function() {
    var searchString = $("#input-search").val();

    $.ajax({
        type:'GET',
        url:'search?searchstring='+searchString,
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            $("#result").html(data);
        }
    });
});

$("#btn-group-update").click(function(){
    var groupToUpdate = {
        name:$("#input-group-name-update").val(),
        currency:$("#select-group-currency-update option:selected" ).text()
    };

    $.ajax({
        type:'POST',
        url:'group/update?oldname='+$("#group-header").text(),
        contentType:'application/json; charset=utf-8',
        data: JSON.stringify(groupToUpdate),
        success: function (data) {
            console.log(data);
        }
    });

    processUpload();
});

$("#btn-member-update").click(function(){
    var memberToUpdate = {
        firstName:$("#editMemberFirstname").val(),
        lastName:$("#editMemberLastname" ).val(),
        address:$("#editMemberAddress" ).val()
    };

    $.ajax({
        type:'POST',
        url:'group/update/member?oldaddress='+memberUpdateOldAddress,
        contentType:'application/json; charset=utf-8',
        data: JSON.stringify(memberToUpdate),
        success: function (data) {
            console.log(data);
            memberUpdateOldAddress = "";
        }
    });
    $("#editMemberModal").modal('toggle');
});

$("#btn-member-delete").click(function(){
    var memberToDelete = {
        address:memberDeleteAddress
    };

    $.ajax({
        type:'DELETE',
        url:'group/delete/member?groupname='+$("#group-header").text(),
        contentType:'application/json; charset=utf-8',
        data: JSON.stringify(memberToDelete),
        success: function (data) {
            console.log(data);
            memberDeleteAddress = "";
        }
    });
    $("#deleteMemberModal").modal('toggle');
});