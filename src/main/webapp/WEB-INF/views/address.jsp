<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Waves Explorer Address</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/line-awesome.min.css">
    <link rel="stylesheet" href="resources/css/bootstrap-select.css">
    <link rel="stylesheet" href="resources/css/datatables.min.css">
    <link rel="stylesheet" href="resources/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="resources/css/scroller.bootstrap4.min.css">
    <link rel="stylesheet" href="resources/css/fixedHeader.bootstrap4.min.css">
    <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
    <link rel="stylesheet" href="resources/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="resources/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="resources/css/customCSS.css">
</head>

<body>

<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/">
        <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
    </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Blocks <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="groups">My Groups</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="statistics">Statistics</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <input id="input-search" class="form-control mr-sm-2 header-search" type="text"
                   placeholder="Search Addres, TX Sig., Block Sig.">
            <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </div>
    </div>
</nav>

<div class="container mt-100 mb-10">
    <div class="row mb-5">
        <div class="col-12 column">
            <h4>Address</h4>
            <div class="info-column">
                <div class="info-table">
                    <div class="info-row">
                        <div class="info-name">Address</div>
                        <div class="info-value">${address.address}</div>
          </div>
          <div class="info-row">
            <div class="info-name">Regular Balance</div>
            <div class="info-value">${address.balance/100000000}</div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 column">
      <h4>Last 50 Transactions</h4>
      <div class="info-column">
        <div class="d-flex justify-content-start align-items-center mb-2">
          <div class="mr-auto pl-3"><i class="la la-filter la-2x"></i></div>
          <div class="pr-3">
            <input class="form-control" id="addressTransactionSearch" placeholder="Search.." />
          </div>
        </div>
        <table class="table table-striped" id="addressTransactionTable">
          <thead>
          <tr>
            <th>ID</th>
            <th>Sender</th>
            <th></th>
            <th>Recipient</th>
            <th>Amount</th>
            <th>Asset</th>
            <th>Type</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <th>ID</th>
            <th>Sender</th>
            <th class="arrow"></th>
            <th>Recipient</th>
            <th>Amount</th>
            <th>Asset</th>
            <th>Type</th>
          </tr>
          </tfoot>
          <tbody>

          <c:forEach items="${transactions}" var="transaction">
            <tr>
              <td><a href="transaction?id=${transaction.transactionId}">${transaction.transactionId}</a></td>
              <td><a href="address?address=${transaction.senderAddress.address}">${transaction.senderAddress.address}</a></td>
              <td>1</td>
              <td><a href="address?address=${transaction.recipientAddress.address}">${transaction.recipientAddress.address}</a></td>
              <td>${transaction.amount/100000000}</td>
              <c:choose>
                <c:when test="${empty transaction.asset.name}">
                  <td>WAVES</td>
                </c:when>
                <c:otherwise>
                  <td>${transaction.asset.name}</td>
                </c:otherwise>
              </c:choose>
              <td>${transaction.type}</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<nav class="navbar fixed-bottom navbar-light bg-faded">
  <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
</nav>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootstrap-select.js"></script>
<script src="resources/js/datatables.min.js"></script>
<script src="resources/js/dataTables.bootstrap4.min.js"></script>
<script src="resources/js/dataTables.fixedHeader.min.js"></script>
<script src="resources/js/dataTables.scroller.min.js"></script>
<script src="resources/js/dataTables.responsive.min.js"></script>
<script src="resources/js/responsive.bootstrap4.min.js"></script>
<script src="resources/js/Chart.min.js"></script>
<script src="resources/js/explorer.js"></script>
</body>
</body>

</html>