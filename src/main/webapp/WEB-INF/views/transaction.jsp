<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Waves Explorer Address</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="resources/css/line-awesome.min.css">
  <link rel="stylesheet" href="resources/css/customCSS.css">
</head>

<body>

<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="/">
    <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Blocks <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="groups">My Groups</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="statistics">Statistics</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <input id="input-search" class="form-control mr-sm-2 header-search" type="text" placeholder="Search Addres, TX Sig., Block Sig.">
      <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </div>
  </div>
</nav>

<div class="container mt-100 mb-10">
  <div class="row mb-5">
    <div class="col-12 column">
      <h4>Transaction</h4>
      <div class="info-column">
        <div class="info-table">
          <div class="info-row">
            <div class="info-name">ID</div>
            <div class="info-value">${transaction.transactionId}</div>
          </div>
          <div class="info-row">
            <div class="info-name">Type</div>
            <div class="info-value">${transaction.type}</div>
          </div>
          <div class="info-row">
            <div class="info-name">Timestamp</div>
            <div class="info-value"><fmt:formatDate type = "both" value = "${transaction.timestamp.time}" /></div>
          </div>
          <div class="info-row">
            <div class="info-name">Block</div>
            <div class="info-value"><a href="block?height=${transaction.block.height}">${transaction.block.height}</a></div>
          </div>
          <div class="info-row">
            <div class="info-name">Asset</div>
            <div class="info-value">
              <c:choose>
                <c:when test="${empty transaction.asset.name}">
                  <td>WAVES</td>
                </c:when>
                <c:otherwise>
                  <td>${transaction.asset.name}</td>
                </c:otherwise>
              </c:choose>
            </div>
          </div>
          <div class="info-row">
            <div class="info-name">Amount</div>
            <div class="info-value">${transaction.amount/100000000}</div>
          </div>
          <div class="info-row">
            <div class="info-name">Fee</div>
            <div class="info-value">${transaction.fee/1000000000}</div>
          </div>
          <div class="info-row">
            <div class="info-name">Sender</div>
            <div class="info-value"><a href="address?address=${transaction.senderAddress.address}">${transaction.senderAddress.address}</a></div>
          </div>
          <div class="info-row">
            <div class="info-name">Recipient</div>
            <div class="info-value"><a href="address?address=${transaction.recipientAddress.address}">${transaction.recipientAddress.address}</a></div>
          </div>
          <div class="info-row">
            <div class="info-name">Signature</div>
            <div class="info-value">${transaction.signature}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<nav class="navbar fixed-bottom navbar-light bg-faded">
  <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
</nav>


<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/explorer.js"></script>
</body>

</html>
