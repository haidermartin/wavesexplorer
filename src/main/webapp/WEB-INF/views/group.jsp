<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Waves Explorer Block</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="resources/css/line-awesome.min.css">
  <link rel="stylesheet" href="resources/css/bootstrap-select.css">
  <link rel="stylesheet" href="resources/css/datatables.min.css">
  <link rel="stylesheet" href="resources/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/scroller.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/fixedHeader.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
  <link rel="stylesheet" href="resources/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="resources/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
  <link rel="stylesheet" href="resources/css/customCSS.css">

</head>

<body>

  <nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <a class="navbar-brand" href="#">
      <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
    </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/">Blocks</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="groups">My Groups <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="statistics">Statistics</a>
        </li>
      </ul>
      <div class="form-inline my-2 my-lg-0">
        <input id="input-search" class="form-control mr-sm-2 header-search" type="text" placeholder="Search Addres, TX Sig., Block Sig.">
        <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </div>
    </div>
  </nav>

  <div class="container mt-100 mb-10">
    <div class="row mb-5">
      <div class="col-12 column">
        <h4 id="group-header">${group.name}</h4>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#groupOverviewTab" role="tab">Group</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#groupTransactionsTab" role="tab">Transactions</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#groupMembersTab" role="tab">Members</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#groupStatisticsTab" role="tab">Statistics</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#groupSettingsTab" role="tab">Settings</a>
          </li>
        </ul>
        <div class="tab-content info-column">
          <div class="tab-pane active" id="groupOverviewTab" role="tabpanel">
            <div class="info-table">
              <div class="info-row">
                <div class="info-name">
                  Members
                </div>
                <div class="info-value">
                  ${fn:length(group.members)}
                </div>
              </div>
              <div class="info-row">
                <div class="info-name">
                  Transaction Volume
                </div>
                <div class="info-value">
                  ${totalAmount/100000000}
                </div>
              </div>
              <div class="info-row">
                <div class="info-name">
                  Transactions
                </div>
                <div class="info-value">
                  ${fn:length(transactions)}
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="groupTransactionsTab" role="tabpanel">
            <div class="d-flex justify-content-start align-items-center mb-2">
              <div class="pr-3 pl-3"><i class="la la-filter la-2x"></i></div>
              <div class="pr-3">
                <select class="selectpicker show-tick" title="Select Period">
                  <option>All</option>
                  <option>Today</option>
                  <option>Yesterday</option>
                  <option>This Week</option>
                  <option>Last Week</option>
                  <option>This Month</option>
                  <option>Last Month</option>
                  <option>This Year</option>
                  <option>Last Year</option>
                </select>
              </div>
              <div class="mr-auto">
                <select class="selectpicker show-tick" multiple title="Select Scope">
                  <option selected>All</option>
                  <option>Extern In</option>
                  <option>Extern Out</option>
                </select>
              </div>
              <div class="pr-3">
                <input class="form-control" id="groupTransactionSearch" placeholder="Search.." />
              </div>
            </div>
            <table class="table table-striped" id="groupTransactionTable">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Sender</th>
                  <th>Recipient</th>
                  <th>Amount</th>
                  <th>Asset</th>
                  <th>Type</th>
                  <th>Timestamp</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Sender</th>
                  <th>Recipient</th>
                  <th>Amount</th>
                  <th>Asset</th>
                  <th>Type</th>
                  <th>Timestamp</th>
                </tr>
              </tfoot>
              <tbody>
                <c:forEach items="${transactions}" var="transaction">
                  <tr>
                    <td><a href="transaction?id=${transaction.transactionId}">${transaction.transactionId}</a></td>
                    <td><a href="address?address=${transaction.senderAddress.address}">${transaction.senderAddress.member.firstName} ${transaction.senderAddress.member.lastName}</a></td>
                    <td><a href="address?address=${transaction.recipientAddress.address}">${transaction.recipientAddress.member.firstName} ${transaction.recipientAddress.member.lastName}</a></td>
                    <td>${transaction.amount/100000000}</td>
                    <c:choose>
                      <c:when test="${empty transaction.asset.name}">
                        <td>WAVES</td>
                      </c:when>
                      <c:otherwise>
                        <td>${transaction.asset.name}</td>
                      </c:otherwise>
                    </c:choose>
                    <td>${transaction.type}</td>
                    <td><fmt:formatDate type = "both" value = "${transaction.timestamp.time}" /></td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="groupMembersTab" role="tabpanel">
            <div class="d-flex justify-content-start align-items-center mb-2">
              <div class="pl-3 mr-auto">
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addMemberModal"><i class="la la-plus-circle la-1x"></i> Add Member</button>
              </div>
              <div class="pr-3">
                <input class="form-control" id="groupMemberSearch" placeholder="Search.." />
              </div>
            </div>
            <table class="table table-striped" id="groupMembersTable">
              <thead>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Address</th>
                <th>Group Balance</th>
                <th>No. Tx(s)</th>
                <th></th>
              </thead>
              <tbody>
              <c:if test="${fn:length(group.members) > '0'}">
                <c:forEach items="${group.members}" var="member">
                  <tr>
                    <td>${member.firstName}</td>
                    <td>${member.lastName}</td>
                    <td>${member.address.address}</td>
                    <td>${member.address.balance/100000000} </td>
                    <td>${fn:length(member.address.senderTransactions) + fn:length(member.address.recipientTransactions)}</td>
                    <td><span class="memberActionIcon memberEdit"><i class="la la-pencil"></i></span><span class="memberActionIcon memberDelete"><i class="la la-trash-o"></i></span></td>
                  </tr>
                </c:forEach>
              </c:if>
              <c:if test="${fn:length(group.members) == '0'}">
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </c:if>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="groupStatisticsTab" role="tabpanel">
            <div class="row pr-3 pl-3">
              <div class="col-12">
                <div class="d-flex justify-content-start align-items-center mb-2">
                  <div class="pr-3"><i class="la la-filter la-2x"></i></div>
                  <select class="selectpicker show-tick" title="Select Period">
                  <option>All Time</option>
                  <option>30 Days</option>
                  <option>60 Days</option>
                  <option>180 Days</option>
                  <option>1 Year</option>
                </select>
                </div>
              </div>
              <div class="col-6">
                <div class="stat-container">
              <div class="d-flex justify-content-between">
                <h5>Transactions per Member (inside Group)</h5>
              </div>
              <canvas id="groupTransactionsPerMember" height="200"></canvas>
            </div>
              </div>
              <div class="col-6">
                <div class="stat-container">
                  <div class="d-flex justify-content-between">
                    <h5>Total Transactions</h5>
                    <span id="id-totaltransactions-group-span">10</span>
                    <!-- Total of Period -->
                  </div>
                  <canvas id="groupTotalTransactions" height="200"></canvas>
                </div>
              </div>
              <div class="col-6">
                <div class="stat-container">
                  <h5>Member Balance</h5>
                  <canvas id="groupBalancePerMember" height="200"></canvas>
                </div>
              </div>
              <div class="col-6">
                <div class="stat-container">
                  <div class="d-flex justify-content-between">
                    <h5>Group Balance</h5>
                    <span>120</span>
                    <!-- Total of Period -->
                  </div>
                  <canvas id="groupBalance" height="200"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="groupSettingsTab" role="tabpanel">
            <div class="info-table">
              <div class="info-row">
                <div class="info-name">
                  Groupname
                </div>
                <div class="info-value">
                  <div class="input-group">
                    <input id="input-group-name-update" type="text" class="form-control" placeholder="Groupname" value="${group.name}">
                  </div>
                </div>
              </div>
              <div class="info-row">
                <div class="info-name">
                  Currency
                </div>
                <div class="info-value">
                  <div class="input-group">
                    <select id="select-group-currency-update" class="selectpicker show-tick" data-live-search="true" data-width="300px">
                      <option selected>WAVES</option>
                      <c:forEach items="${assets}" var="asset">
                        <option>${asset.name}</option>
                      </c:forEach>
                    </select>
                  </div>
                </div>
              </div>
              <div class="info-row">
                <div class="info-name">
                  Upload Excel-Sheet
                </div>
                <div class="info-value">
                  <div class="input-group">
                    <input type="file" class="form-control-file" id="memberInputFile" aria-describedby="fileHelp">
                  </div>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-end mt-3">
              <div class="pr-3">
                <button id="btn-group-update" type="button" class="btn btn-outline-success">Save Changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar fixed-bottom navbar-light bg-faded">
    <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
  </nav>

  <!-- edit Member Modal -->
  <div class="modal fade" id="editMemberModal" tabindex="-1" role="dialog" aria-labelledby="editMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editMemberLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Firstname" id="editMemberFirstname">
          </div>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Lastname" id="editMemberLastname">
          </div>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Address" id="editMemberAddress">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button id="btn-member-update" type="button" class="btn btn-outline-success">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <!-- delete Member Modal -->
  <div class="modal fade" id="deleteMemberModal" tabindex="-1" role="dialog" aria-labelledby="deleteMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteMemberLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete <span id="deleteMemberName"></span> from the group?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <button id="btn-member-delete" type="button" class="btn btn-outline-success">Yes</button>
        </div>
      </div>
    </div>
  </div>

  <!-- add Group Member Modal -->
  <div class="modal fade" id="addMemberModal" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><!--id="addMemberModal"-->Add Member</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <input id="input-member-firstname" type="text" class="form-control" placeholder="Firstname">
          </div>
          <div class="input-group">
            <input id="input-member-lastname" type="text" class="form-control" placeholder="Lastname">
          </div>
          <div class="input-group">
            <input id="input-member-address" type="text" class="form-control" placeholder="Address">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button id="btn-add-member" type="button" class="btn btn-outline-success">Add Member</button>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/bootstrap-select.js"></script>
  <script src="resources/js/datatables.min.js"></script>
  <script src="resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="resources/js/dataTables.fixedHeader.min.js"></script>
  <script src="resources/js/dataTables.scroller.min.js"></script>
  <script src="resources/js/dataTables.responsive.min.js"></script>
  <script src="resources/js/responsive.bootstrap4.min.js"></script>
  <script src="resources/js/dataTables.rowGroup.min.js"></script>
  <script src="resources/js/chart.min.js"></script>
  <script src="resources/js/explorer.js"></script>
  <script src="resources/js/explorer.group.statistics.js"></script>
</body>

</html>
