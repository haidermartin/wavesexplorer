<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Waves Explorer</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="resources/css/line-awesome.min.css">
  <link rel="stylesheet" href="resources/css/bootstrap-select.css">
  <link rel="stylesheet" href="resources/css/datatables.min.css">
  <link rel="stylesheet" href="resources/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/scroller.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/fixedHeader.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
  <link rel="stylesheet" href="resources/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="resources/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/customCSS.css">
</head>

<body>
<div id="result">
<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">
    <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Blocks <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="groups">My Groups</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="statistics">Statistics</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <input id="input-search" class="form-control mr-sm-2 header-search" type="text" placeholder="Search Addres, TX Sig., Block Sig.">
      <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </div>
  </div>
</nav>

<div class="container mt-100 mb-10">
  <div class="row">
    <div class="col-12 column">
      <h4>Latest Blocks</h4>
      <div class="info-column">
        <table class="table table-striped" id="latestBlocksTable">
          <thead>
          <tr>
            <th>Height</th>
            <th>Timestamp</th>
            <th>Generator</th>
            <th>Size (bytes)</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${blocks}" var="block">
              <tr id="${block.height}">
                <td><a href="block?height=${block.height}">${block.height}</a></td>
                <td><fmt:formatDate type = "both" value = "${block.timestamp.time}" /></td>
                <td><a href="address?address=${block.generator.address}">${block.generator.address}</a></td>
                <td>${block.blocksize}</td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>

<nav class="navbar fixed-bottom navbar-light bg-faded">
  <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
</nav>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootstrap-select.js"></script>
<script src="resources/js/datatables.min.js"></script>
<script src="resources/js/dataTables.bootstrap4.min.js"></script>
<script src="resources/js/dataTables.fixedHeader.min.js"></script>
<script src="resources/js/dataTables.scroller.min.js"></script>
<script src="resources/js/dataTables.responsive.min.js"></script>
<script src="resources/js/responsive.bootstrap4.min.js"></script>
<script src="resources/js/Chart.min.js"></script>
<script src="resources/js/explorer.js"></script>
</body>

</html>