<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Waves Explorer</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="resources/css/line-awesome.min.css">
  <link rel="stylesheet" href="resources/css/bootstrap-select.css">
  <link rel="stylesheet" href="resources/css/customCSS.css">
</head>

<body>

  <nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <a class="navbar-brand" href="#">
      <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
    </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/">Blocks</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="groups">My Groups</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="statistics">Statistics <span class="sr-only">(current)</span></a>
        </li>
      </ul>
      <div class="form-inline my-2 my-lg-0">
        <input id="input-search" class="form-control mr-sm-2 header-search" type="text" placeholder="Search Addres, TX Sig., Block Sig.">
        <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </div>
    </div>
  </nav>

  <div class="container mt-100 mb-10">
    <div class="row">
      <div class="col-12 column">
        <h4>Statistics</h4>
        <div class="info-column row">
          <div class="col-12">
            <div class="d-flex justify-content-start align-items-center mb-2">
              <div class="pr-3 pl-3"><i class="la la-filter la-2x"></i></div>
              <select id="stat-period" class="selectpicker show-tick" title="Select Period">
                  <option value="0" selected>All Time</option>
                  <option value="30">30 Days</option>
                  <option value="60">60 Days</option>
                  <option value="180">180 Days</option>
                  <option value="365">1 Year</option>
                </select>
            </div>
          </div>
          <div class="col-6">
            <div class="stat-container">
              <div class="d-flex justify-content-between">
                <h5>Avg. Block Size</h5>
                <span id="id-avgblocksize-span"></span>
                <!-- Avg of Period -->
              </div>
              <canvas id="statsAvgBlockSize" height="200"></canvas>
            </div>
          </div>
          <div class="col-6">
            <div class="stat-container">
              <div class="d-flex justify-content-between">
                <h5>Total Transactions</h5>
                <span id="id-avgtransactions-span"></span>
                <!-- Total of Period -->
              </div>
              <canvas id="statsTotalTransactions" height="200"></canvas>
            </div>
          </div>
          <div class="col-6">
            <div class="stat-container">
              <h5>Transaction Types</h5>
              <canvas id="statsTransactionTypes" height="200"></canvas>
            </div>
          </div>
          <div class="col-6">
            <div class="stat-container">
              <div class="d-flex justify-content-between">
                <h5>Avg. Transactions per Block</h5>
                <span id="id-avgtransactionsblock-span"></span>
                <!-- Total of Period -->
              </div>
              <canvas id="statsTransactionsPerBlock" height="200"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar fixed-bottom navbar-light bg-faded">
    <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
  </nav>

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/bootstrap-select.js"></script>
  <script src="resources/js/chart.min.js"></script>
  <script src="resources/js/explorer.js"></script>
  <script src="resources/js/explorer.statistics.js"></script>
</body>

</html>
