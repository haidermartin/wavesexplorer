<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Waves Explorer</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="resources/css/line-awesome.min.css">
  <link rel="stylesheet" href="resources/css/bootstrap-select.css">
  <link rel="stylesheet" href="resources/css/datatables.min.css">
  <link rel="stylesheet" href="resources/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/scroller.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/fixedHeader.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
  <link rel="stylesheet" href="resources/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="resources/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/css/rowGroup.bootstrap4.css">
  <link rel="stylesheet" href="resources/css/customCSS.css">
</head>

<body>

<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="/">
    <img class="waves-logo" src="resources/img/waves-logo.png" alt="">
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Blocks</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="groups">My Groups <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="statistics">Statistics</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <input id="input-search" class="form-control mr-sm-2 header-search" type="text" placeholder="Search Addres, TX Sig., Block Sig.">
      <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </div>
  </div>
</nav>

<div class="container mt-100 mb-5">
  <div class="row">
    <div class="col-12 column">
      <h4>My Groups</h4>
      <div class="info-column">
        <div class="d-flex justify-content-start align-items-center mb-2">
          <div class="pl-3 mr-auto">
            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addGroupModal"><i class="la la-plus-circle la-1x"></i> Add Group</button>
          </div>
          <div class="pr-3">
            <input class="form-control" id="groupsSearch" placeholder="Search.." />
          </div>
        </div>
        <table class="table table-striped" id="groupsTable">
          <thead>
          <tr>
            <th>Group Name</th>
            <th>Latest Transaction</th>
            <th>Transaction Volume</th>
            <th>Members</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${groups}" var="group">
            <tr>
              <td><a href="group?name=${group.name}">${group.name}</a></td>
              <td><fmt:formatDate type = "both" value = "${group.latestTransactionDate.time}" /></td>
              <td>${group.transactionVolume/100000000}</td>
              <td>${fn:length(group.members)}</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<nav class="navbar fixed-bottom navbar-light bg-faded">
  <p>Waves Explorer &copy; 2017 Haider, Eder, Peilsteiner, Bernert</p>
</nav>

<!-- create group modal box -->
<div class="modal fade" id="addGroupModal" tabindex="-1" role="dialog" aria-labelledby="addGroupLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addGroupLabel">Create Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input id="input-group-name" type="text" class="form-control" placeholder="Group Name">
        </div>
        <label>Group Currency</label>
        <div class="input-group">
          <select id="select-group-currency" class="selectpicker show-tick" data-live-search="true" data-width="auto">
            <option selected>WAVES</option>
            <c:forEach items="${assets}" var="asset">
              <option>${asset.name}</option>
            </c:forEach>

          </select>
        </div>
        <label>Select Group Parent</label>
        <div class="input-group">
          <select id="select-group-parent" class="selectpicker show-tick" data-live-search="true" data-width="auto">
            <option selected>None</option>
            <c:forEach items="${groups}" var="group">
              <option>${group.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="btn-add-group" type="button" class="btn btn-outline-success">Save Group</button>
      </div>
    </div>
  </div>
</div>
  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/bootstrap-select.js"></script>
  <script src="resources/js/datatables.min.js"></script>
  <script src="resources/js/dataTables.bootstrap4.min.js"></script>
  <script src="resources/js/dataTables.fixedHeader.min.js"></script>
  <script src="resources/js/dataTables.scroller.min.js"></script>
  <script src="resources/js/dataTables.responsive.min.js"></script>
  <script src="resources/js/responsive.bootstrap4.min.js"></script>
  <script src="resources/js/dataTables.rowGroup.min.js"></script>
<script src="resources/js/explorer.js"></script>
</body>

</html>